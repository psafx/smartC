﻿#include "cmd.h"
#include "ui_cmd.h"
#include<QDebug>
#include<QMessageBox>
#include<sstream>//将字符串拆分所用到的库 字符串流
#include<QTextBlock>
#include<QSqlQueryModel>
using namespace std;
#pragma execution_character_set("utf-8")
cmd::cmd(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cmd)
{
    ui->setupUi(this);
    setFixedSize(852,575);
    connect(ui->textEdit,SIGNAL(cursorPositionChanged()),this,SLOT(setCursor()));//命令行强制只有最后一行可以编辑
    ui->textEdit->append("command:\n");
    ui->textEdit->setFocusPolicy(Qt::StrongFocus);
        ui->textEdit->setFocusPolicy(Qt::NoFocus);

        ui->textEdit->setFocus();//重设光标
        ui->textEdit->installEventFilter(this);//设置完后自动调用其eventFilter函数


}

cmd::~cmd()
{
    delete ui;
}
void cmd::setCursor()//将光标移动到最后一行
{

    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->textEdit->setTextCursor(cursor);//光标移动到最后一行
}
bool cmd::eventFilter(QObject *target, QEvent *event)
{
    if(target == ui->textEdit)
    {
        if(event->type() == QEvent::KeyPress)//回车键
        {
             QKeyEvent *k = static_cast<QKeyEvent *>(event);
             if(k->key() == Qt::Key_Return)
             {
                 send();
                 return true;
             }
        }
    }
    return QWidget::eventFilter(target,event);
}
void cmd::send()
{

    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::End);

    ui->textEdit->setTextCursor(cursor);//光标先移动到最后一行
    QTextDocument *textDocument= ui->textEdit->document();
    int lineNumber = cursor.blockNumber();//行号
    QTextBlock textBlock =textDocument->findBlockByLineNumber(lineNumber);
    QString selectLine=textBlock.text();
    stringstream input(selectLine.toStdString());//QString转string
    QString result;
    string sresult;//对于string与QString的不兼容处理
    while(input>>sresult)
    {
        result=QString::fromStdString(sresult);
        command.push_back(result);//此时vector会依次存放单词

    }
    //开始处理指令
    judge();
    ui->textEdit->append("command:");
    ui->textEdit->append("");

    command.clear();//清空，便于下一条指令获取
    input.clear();
}
void cmd::judge()//处理指令
{
    int length;
    length=command.size();
    switch (length) {
    case 0:
        ui->textEdit->append("C++智能资料库v1.3");
        break;
    case 1:
        if(command[0]=="exit")
        {
            exit(0);
        }
        else if(command[0]=="cdex")
        {
            a.cdexshow();
        }
        else if(command[0]=="function")
        {
            a.funcshow();
        }
        else if(command[0]=="createcode")
        {
            a.createshow();
        }
        else if(command[0]=="test")
        {
            a.questionshow();
        }
        else if(command[0]=="struct")
        {
            a.shujushow();
        }
        else if(command[0]=="close")
        {
            close();
        }
        else
        {
            ui->textEdit->append("no such command");
        }
        break;
    case 2:
        if(command[0]=="cat")
        {
            QSqlQueryModel *model=new QSqlQueryModel;
            QString cat=command[1];
            QString sql1,sql2,sql3,sql4;
            int number;//搜索结果
            QModelIndex index;
            QString message;
            sql1="select count(*) from function where name ='"+cat+"'";
            sql2="select count(*) from yufa where name ='"+cat+"'";
            sql3="select count(*) from shuju where name ='"+cat+"'";
            sql4="select count(*) from head where name ='"+cat+"'";
            model->setQuery(sql1);
            index=model->index(0,0);
            number=index.data().toInt();
            QString sqlFinal;
            if(number!=0)//表中有此数据
            {
                sqlFinal="select word from function where name='"+cat+"'";
                model->setQuery(sqlFinal);
                index=model->index(0,0);//根据自己的数据库结构来写

                message=index.data().toString();
                QMessageBox::about(NULL,cat,message);//cat是消息框名，message是要显示的内容
                break;
            }
            model->setQuery(sql2);
            index=model->index(0,0);
             number=index.data().toInt();
            if(number!=0)//表中有此数据
            {
                sqlFinal="select word from yufa where name='"+cat+"'";
                model->setQuery(sqlFinal);
                index=model->index(0,0);
                message=index.data().toString();
                QMessageBox::about(NULL,cat,message);
                break;
            }
            model->setQuery(sql3);
            index=model->index(0,0);
             number=index.data().toInt();
            if(number!=0)//表中有此数据
            {
                sqlFinal="select word from shuju where name='"+cat+"'";
                model->setQuery(sqlFinal);
                index=model->index(0,0);
                message=index.data().toString();
                QMessageBox::about(NULL,cat,message);
                break;
            }
            model->setQuery(sql4);
            index=model->index(0,0);
            number=index.data().toInt();
            if(number!=0)//表中有此数据
            {
                sqlFinal="select code from head where name='"+cat+"'";
                model->setQuery(sqlFinal);
                index=model->index(0,0);
                message=index.data().toString();
                QMessageBox::about(NULL,cat,message);
                break;
            }
            if(number==0)
            {
                ui->textEdit->append("cat:no such information");
                break;
            }
        }
        break;
    default:
        ui->textEdit->append("no such command");
        break;
    }
}
