﻿#ifndef KNN_HEAD_H
#define KNN_HEAD_H
#include"knn.h"
#include <QMainWindow>

namespace Ui {
class knn_head;
}

class knn_head : public QMainWindow
{
    Q_OBJECT

public:
    explicit knn_head(QWidget *parent = 0);
    ~knn_head();

private slots:
    void on_pushButton_clicked();

private:
    Ui::knn_head *ui;
};

#endif // KNN_HEAD_H
