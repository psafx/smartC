﻿#include "knn_head.h"
#include "ui_knn_head.h"

knn_head::knn_head(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::knn_head)
{
    ui->setupUi(this);
    KNN knn;
    knn.get_all_distance();
        knn.show();
        knn.get_max_fre_label();

    knn.fin.close();
}

knn_head::~knn_head()
{
    delete ui;
}

void knn_head::on_pushButton_clicked()
{
    close();
}
