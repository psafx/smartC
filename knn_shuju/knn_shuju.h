﻿#ifndef KNN_SHUJU_H
#define KNN_SHUJU_H
#include"knn.h"
#include <QMainWindow>

namespace Ui {
class knn_shuju;
}

class knn_shuju : public QMainWindow
{
    Q_OBJECT

public:
    explicit knn_shuju(QWidget *parent = 0);
    ~knn_shuju();

private slots:
    void on_pushButton_clicked();

private:
    Ui::knn_shuju *ui;
};

#endif // KNN_SHUJU_H
