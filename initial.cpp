﻿#include "initial.h"
#include "ui_initial.h"
#include<QSettings>
#include<QSqlQueryModel>
initial::initial(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::initial)
{
    ui->setupUi(this);
    setFixedSize(445,260);//设置窗口不可拉伸
}

initial::~initial()
{
    delete ui;
}

void initial::on_pushButton_no_clicked()
{
    close();
}

void initial::on_pushButton_yes_clicked()
{
    init_ini();
}
void initial::init_ini()
{
    QSettings  m_IniFile("./data.ini", QSettings::IniFormat);


    m_IniFile.beginGroup("cdex");// 设置当前节名，代表以下的操作都是在这个节中
    m_IniFile.setValue( "for",  "0");// 因为上面设置了节了，这里不在需要把节名写上去
    m_IniFile.setValue( "if",  "0");
    m_IniFile.setValue( "while",  "0");
    m_IniFile.setValue( "switch",  "0");
    m_IniFile.setValue( "struct",  "0");
    m_IniFile.setValue( "namespace",  "0");
    m_IniFile.setValue( "new",  "0");
    m_IniFile.setValue( "union",  "0");
    m_IniFile.setValue( "array",  "0");
    m_IniFile.setValue( "enum",  "0");

    m_IniFile.setValue( "pointer",  "0");
    m_IniFile.setValue( "const",  "0");
    m_IniFile.setValue( "static",  "0");
    m_IniFile.setValue( "define",  "0");
    m_IniFile.endGroup();// 结束当前节的操作
    m_IniFile.beginGroup("function");
    m_IniFile.setValue( "wucanhanshu","0");
    m_IniFile.setValue( "daicanhanshu","0");
    m_IniFile.setValue( "yinyong","0");
    m_IniFile.setValue( "zhizhenhanshu","0");
    m_IniFile.setValue( "xuhanshu","0");
    m_IniFile.setValue( "chunxuhanshu","0");
    m_IniFile.setValue( "diguihanshu","0");
    m_IniFile.setValue( "gouzaohanshu","0");
    m_IniFile.setValue( "xigouhanshu","0");
    m_IniFile.setValue( "lei","0");
    m_IniFile.setValue( "jilei","0");
    m_IniFile.setValue( "youyuan","0");
    m_IniFile.setValue( "chongzai","0");
    m_IniFile.endGroup();
    m_IniFile.beginGroup("shuju");//开始对数据结构的操作
    m_IniFile.setValue( "shunxubiao","0");
    m_IniFile.setValue( "danlianbiao","0");
    m_IniFile.setValue( "shuanglianbiao","0");
    m_IniFile.setValue( "zhan","0");
    m_IniFile.setValue( "duilie","0");
    m_IniFile.setValue( "dui","0");
    m_IniFile.setValue( "jiandanshu","0");
    m_IniFile.setValue( "erchashu","0");
    m_IniFile.setValue( "hongheishu","0");
    m_IniFile.setValue( "AVL","0");
    m_IniFile.setValue( "huffman","0");
    m_IniFile.setValue( "xunhuanlianbiao","0");
    m_IniFile.endGroup();//结束
    m_IniFile.beginGroup("head");
    m_IniFile.setValue( "stdio","0");
    m_IniFile.setValue( "string","0");
    m_IniFile.setValue( "iostream","0");
    m_IniFile.setValue( "vector","0");
    m_IniFile.setValue( "cstdlib","0");
    m_IniFile.setValue( "map","0");
    m_IniFile.setValue( "cctype","0");
    m_IniFile.setValue( "iomanip","0");
    m_IniFile.setValue( "cmath","0");
    m_IniFile.endGroup();
    QSqlQueryModel *model=new QSqlQueryModel;
    model->setQuery("update knn set x=0,y=0,sum=0");//对用户的习惯进行处理
    close();
}
