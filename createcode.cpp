﻿#include "createcode.h"
#include "ui_createcode.h"
#include<QPixmap>
#include"author.h"
#include<QLabel>
#include<QClipboard>
#include<QTimer>
#include<QMessageBox>

#pragma execution_character_set("utf-8")//设置中文= =
createcode::createcode(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::createcode)
{
    ui->setupUi(this);
    QPixmap pixmap = QPixmap("./image/cdex.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    setFixedSize(800,600);
    connect(ui->actionC_yufa,SIGNAL(triggered(bool)),this,SLOT(showcdex()));
    connect(ui->actionC_head,SIGNAL(triggered(bool)),this,SLOT(showhead()));
    connect(ui->action_func,SIGNAL(triggered(bool)),this,SLOT(showfunc()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(showshuju()));
    connect(ui->action_test,SIGNAL(triggered(bool)),this,SLOT(showtest()));
    ui->actionC_yufa->setIcon(QIcon("./image/opencdex.ico"));
    ui->actionC_head->setIcon(QIcon("./image/openhead.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_func->setIcon(QIcon("./image/create.ico"));
    ui->action_test->setIcon(QIcon("./image/exercise1.ico"));
    ui->pushButton_2arrayshow->setIcon(QIcon("./image/enter.ico"));
    ui->stackedWidget->setCurrentIndex(0);//初始化设置到第一个页面
    //list=new QListWidget(this);
    //list->insertItem(0,"array");
    //connect(ui->listWidget_auto,SIGNAL(currentRowChanged(int)),ui->stackedWidget,SLOT(on_listWidget_auto_itemClicked(QListWidgetItem *item)));

}

createcode::~createcode()
{
    delete ui;
}

void createcode::on_pushButton_clicked()
{
    close();
}

void createcode::on_listWidget_auto_itemClicked(QListWidgetItem *item)
{
    int row=ui->listWidget_auto->currentRow();
    if(row==0)
    {
        ui->stackedWidget->setCurrentIndex(0);
    }
    else if(row==1)
    {
        ui->stackedWidget->setCurrentIndex(1);
    }
    else if(row==2)
    {
        ui->stackedWidget->setCurrentIndex(2);
    }
    else if(row==3)
    {
        ui->stackedWidget->setCurrentIndex(3);
    }
    else if(row==4)
    {
        ui->stackedWidget->setCurrentIndex(4);
        maopao_show();
        showfinished();
    }
    else if(row==5)
    {
        ui->stackedWidget->setCurrentIndex(5);
        kuaisu_show();
        showfinished();
    }
    else if(row==6)
    {
        ui->stackedWidget->setCurrentIndex(6);
        xuanze_show();
        showfinished();
    }
    else if(row==7)
    {
        ui->stackedWidget->setCurrentIndex(7);
        charu_show();
        showfinished();
    }
    else if(row==8)
    {
        ui->stackedWidget->setCurrentIndex(8);
        sumsort_show();
        showfinished();
    }
    else if(row==9)
    {
        ui->stackedWidget->setCurrentIndex(9);
        xier_show();
        showfinished();
    }
    else if(row==10)
    {
        ui->stackedWidget->setCurrentIndex(10);
        tong_show();
        showfinished();
    }
    else if(row==11)
    {
        ui->stackedWidget->setCurrentIndex(11);
        rand10();
        showfinished();
    }
}

void createcode::on_pushButton_showarray_clicked()
{
    QString name,length;
    name=ui->lineEdit_arrayname->text();
    length=ui->lineEdit_arraylength->text();

    ui->textEdit->setText("");
    ui->textEdit->append("string "+name+";");
    ui->textEdit->append("cin>>"+length+";");
    ui->textEdit->append("int *"+name+"=new int["+length+"];");
    ui->textEdit->append("delete[] "+name+";");
    QClipboard *clipboard=QApplication::clipboard();
    QString originaltext=clipboard->text();
    clipboard->setText("string "+name+";"+"\n"+"cin>>"+length+";"+"\n"+"int *"+name+"=new int["+length+"];"+"\n"+"delete[] "+name+";");
    showfinished();
}

void createcode::on_pushButton_2arrayshow_clicked()
{
    QString name,length1,length2;

    name=ui->lineEdit_array2name->text();
    length1=ui->lineEdit_1length->text();
    length2=ui->lineEdit_2length->text();
    ui->textEdit_2array->setText("");
    ui->textEdit_2array->append("string "+name+";");
    ui->textEdit_2array->append("int "+length1+";");
    ui->textEdit_2array->append("int "+length2+";");
    ui->textEdit_2array->append("int **"+name+"=new int * ["+length1+"];");
    ui->textEdit_2array->append("for(int i<0;i<"+length1+";i++)");
    ui->textEdit_2array->append("{");
    ui->textEdit_2array->append(name+"[i]=new int["+length2+"];");
    ui->textEdit_2array->append("}");
    ui->textEdit_2array->append("for(int i=0;i<"+length1+";i++)");
    ui->textEdit_2array->append("{");
    ui->textEdit_2array->append("delete[] "+name+"[i];");
    ui->textEdit_2array->append("}");
    ui->textEdit_2array->append(name+"=NULL;");
    QString a,b,c,d,e;
    a="string "+name+";\n"+"int "+length1+";"+"\nint "+length2+";";
    b="int **"+name+"=new int * ["+length1+"];\n"+"for(int i<0;i<"+length1+";i++)\n";
    c="{\n"+name+"[i]=new int["+length2+"];\n"+"}\n";
    d="for(int i=0;i<"+length1+";i++)\n"+"{\n"+"delete[] "+name+"[i];\n"+"}\n";
    e=name+"=NULL;";
    QClipboard *clipboard=QApplication::clipboard();
    clipboard->setText(a+b+c+d+e);
    showfinished();
}

void createcode::on_pushButton_random_clicked()
{
    QString name,length;
    name=ui->lineEdit_randomname->text();
    length=ui->lineEdit_randomlength->text();
    ui->textEdit_random->setText("");
    ui->textEdit_random->append("srand(time(0));");
    ui->textEdit_random->append("int "+name+";");
    ui->textEdit_random->append(name="rand()%(10^"+length+");");
    QString a,b,c;
    a="srand(time(0));\n";
    b="int "+name+";\n";
    c=name+"= rand()%(10^"+length+");";
    QClipboard *clipboard=QApplication::clipboard();
    clipboard->setText(a+b+c);
    showfinished();
}
void createcode::showcdex()
{
    hide();
    author a;
    a.cdexshow();

}
void createcode::showshuju()
{
    hide();
    author a;
    a.shujushow();
}
void createcode::showfunc()
{
    hide();
    author a;
    a.funcshow();
}
void createcode::showhead()
{
    hide();
    author a;
    a.headinfoshow();
}
void createcode::showfinished()
{


   remi=new remind;
   remi->show();


}

void createcode::on_pushButton_sum_clicked()
{
    QString first,end;
    first=ui->lineEdit_chushi->text();
    end=ui->lineEdit_zuizhong->text();
    ui->textEdit_sum->setText("");
    ui->textEdit_sum->append("int sum=0;");
    ui->textEdit_sum->append("int first="+first+";");
    ui->textEdit_sum->append("int end="+end+";");
    ui->textEdit_sum->append("for(int i="+first+";i<="+end+";i++)");
    ui->textEdit_sum->append("{");
    ui->textEdit_sum->append("sum+=i;");
    ui->textEdit_sum->append("}");
    QString a1,a2,a3,b,c;
    a1="int sum=0;\n";
    a2="int first="+first;
    a3=";\n";
    b="int end="+end+";\n"+"for(int i=fisrt;i<=end;i++)\n";
    c="sum+=i;";
    QClipboard *clipboard=QApplication::clipboard();
    clipboard->setText(a1+a2+a3+b+c);//将它们拼在一起会报错，原因不明确。
    showfinished();
}
void createcode::showtest()
{
    hide();
    author a;
    a.questionshow();
}
void createcode::maopao_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='冒泡排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_maopao->setText(sort);
}
void createcode::charu_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='插入排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_charu->setText(sort);

}
void createcode::kuaisu_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='快速排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_kuaisu->setText(sort);
}
void createcode::xuanze_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='选择排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_xuanze->setText(sort);
}
void createcode::sumsort_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='计数排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_sumsort->setText(sort);
}
void createcode::xier_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='希尔排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_jishu->setText(sort);
}
void createcode::tong_show()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='桶排序'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_banket->setText(sort);
}
void createcode::rand10()
{
    model=new QSqlQueryModel;
    model->setQuery("select data from code where name='生成10个随机数'");
    index=model->index(0,0);
    sort=index.data().toString();
    ui->textEdit_rand10->setText(sort);
}
