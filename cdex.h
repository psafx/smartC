﻿#ifndef CDEX_H
#define CDEX_H
#pragma execution_character_set("utf-8")
#include <QMainWindow>
#include <QStandardItemModel>
#include<QStringLiteral>
#include<QString>
#include<QSqlDatabase>
#include<QSqlQuery>
#include"function.h"
#include<QDebug>
namespace Ui {
class cdex;
}

class cdex : public QMainWindow
{
    Q_OBJECT

public:
    explicit cdex(QWidget *parent = 0);
    ~cdex();
    void invite_set();//cdex的智能推荐功能
    void knn_cdex();//cdex的knn算法信息收集
    void knn_show();//knn结果处理
    void knn_judge();//根据结果标签进行判断，随后展示
    void knn_cdex_init();//对knn的结果进行处理
private slots:
    void Show();
    void on_pushButton_exit_clicked();

    void on_pushButton_find_clicked();

    void init();
    void WHILE();

    void FOR();
    void on_tableView_clicked(const QModelIndex &index);

    void on_lineEdit_find_textChanged(const QString &arg1);
    void IF();
    void SWITCH();
    void CLASS();
    void STRUCT();
    void NAMESPACE();
    void NEW();
    void ENUM();
    void UNION();
    void ARRAY();
    void CONST();
    void STATIC();

    void DEFINE();
    void POINTER();
    void showhead();
    void showshuju();
    void showfunc();
    void showcreate();
    void invite();//通过invite_set判断哪个搜索项次数最多，然后显示对应内容
    void invite_judge();//判断是否执行智能推荐功能
    void showtest();
private:
    Ui::cdex *ui;
    function *func;
    QString maxS;
    int max=0;
    char KN;//knn处理后的结果

};

#endif // CDEX_H
