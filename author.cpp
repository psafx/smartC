﻿#include "author.h"
#include "ui_author.h"
#include"cdex.h"

author::author(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::author)
{

    ui->setupUi(this);
    QPixmap pixmap = QPixmap("./image/author.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    setFixedSize(453,297);//设置窗口不可拉伸

}

author::~author()
{
    delete ui;
}
void author::cdexshow()
{
    Cdex=new cdex;
    Cdex->setWindowFlags(Cdex->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    Cdex->show();

}
void author::headinfoshow()
{
    headin=new headinfo;
    headin->setWindowFlags(headin->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    headin->show();

}

void author::shujushow()
{
    shuju=new shujujiegou;
    shuju->setWindowFlags(shuju->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    shuju->show();
}
void author::funcshow()
{
    func=new function;
    func->setWindowFlags(func->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    func->show();
}
void author::createshow()
{
    create=new createcode;
    create->setWindowFlags(create->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    create->show();
}
void author::questionshow()
{
    ques=new question;
    ques->setWindowFlags(ques->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    ques->show();
}
