﻿#ifndef CREATECODE_H
#define CREATECODE_H
#include<QStackedWidget>
#include <QMainWindow>
#include<QListWidget>
#pragma execution_character_set("utf-8")//设置中文= =
#include<QLabel>
#include<QMessageBox>
#include<QDebug>
#include"remind.h"
#include<QSqlQueryModel>
namespace Ui {
class createcode;
}

class createcode : public QMainWindow
{
    Q_OBJECT

public:
    explicit createcode(QWidget *parent = 0);
    ~createcode();
    QMessageBox msg;
    void maopao_show();//冒泡排序
    void kuaisu_show();
    void charu_show();
    void xuanze_show();
    void sumsort_show();//计数排序
    void xier_show();//希尔排序
    void tong_show();
    void rand10();
private slots:
    void on_pushButton_clicked();

    void on_listWidget_auto_itemClicked(QListWidgetItem *item);

    void on_pushButton_showarray_clicked();

    void on_pushButton_2arrayshow_clicked();

    void on_pushButton_random_clicked();
    void showcdex();
    void showshuju();
    void showfunc();
    void showhead();
    void showfinished();
    void showtest();
    void on_pushButton_sum_clicked();

private:
    Ui::createcode *ui;
    QStackedWidget *stack;
    QListWidget *list;
    remind *remi;
    QSqlQueryModel *model;
    QModelIndex index;
    QString sort;//七个排序算法的代码
};

#endif // CREATECODE_H
