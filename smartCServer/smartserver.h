﻿#ifndef SMARTSERVER_H
#define SMARTSERVER_H

#include <QMainWindow>
#include<QTcpServer>
namespace Ui {
class SmartServer;
}

class SmartServer : public QMainWindow
{
    Q_OBJECT

public:
    explicit SmartServer(QWidget *parent = 0);
    ~SmartServer();

private slots:
    void on_pushButton_setVersion_clicked();
    void sendmassage();//自动发送
private:
    Ui::SmartServer *ui;
    QTcpServer *tcpserver;
    QString version;//版本号
};

#endif // SMARTSERVER_H
