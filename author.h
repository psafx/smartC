﻿#ifndef AUTHOR_H
#define AUTHOR_H
#pragma execution_character_set("utf-8")
#include <QWidget>
#include<cdex.h>
#include"headinfo.h"
#include"createcode.h"
#include"shujujiegou.h"
#include"function.h"
#include"question.h"
namespace Ui {
class author;
}

class author : public QWidget
{
    Q_OBJECT

public:
    explicit author(QWidget *parent = 0);
    ~author();
    void cdexshow();
    void headinfoshow();
    int invi;//0为不启用智能推荐，1为启用
    void shujushow();
    void funcshow();
    void createshow();
    void questionshow();
private:
    Ui::author *ui;
    cdex *Cdex;
    headinfo *headin;
    createcode *create;
    shujujiegou *shuju;
    function *func;
    question *ques;
    int cdex_invi;//cdex的智能推荐，0为启用推荐，1为推荐过了不进行推荐
};

#endif // AUTHOR_H
