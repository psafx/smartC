﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "author.h"
#include<QPixmap>
#include<QSettings>
#include<QDebug>
#include<QDesktopServices>
#include<QUrl>
#include<QApplication>
#include<QSplashScreen>
#include"cmd.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //setStyleSheet("border:2px groove gray;border-radius:10px;padding:2px 4px;");
    connect(ui->actionAuthor,SIGNAL(triggered(bool)),this,SLOT(showauthor()));
    connect(ui->actionC_dex,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_language_clicked()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_shujujiegou_clicked()));
    connect(ui->action_head,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_head_clicked()));
    connect(ui->action_function,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_function_clicked()));
    connect(ui->action_test,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_test_clicked()));
    connect(ui->action_gitee,SIGNAL(triggered(bool)),this,SLOT(openGitee()));
    connect(ui->action_ifupdate,SIGNAL(triggered(bool)),this,SLOT(checkUpdate()));
    QPixmap pixmap = QPixmap("./image/mainwindow.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette.setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));//设置退出按钮的图标
    ui->pushButton_language->setIcon(QIcon("./image/opencdex.ico"));
    ui->pushButton_head->setIcon(QIcon("./image/openhead.ico"));
    ui->pushButton_shujujiegou->setIcon(QIcon("./image/openshuju.ico"));
    ui->pushButton_function->setIcon(QIcon("./image/openfunc.ico"));
    ui->pushButton_test->setIcon(QIcon("./image/exercise1.ico"));
    ui->pushButton_cmd->setIcon(QIcon("./image/cmd.png"));
    ui->actionC_dex->setIcon(QIcon("./image/opencdex.ico"));
    ui->action_head->setIcon(QIcon("./image/openhead.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_function->setIcon(QIcon("./image/openfunc.ico"));
    ui->actionAuthor->setIcon(QIcon("./image/author.ico"));
    ui->pushButton_autocreate->setIcon(QIcon("./image/create.ico"));
    ui->pushButton_init->setIcon(QIcon("./image/cle.ico"));
    ui->action_gitee->setIcon(QIcon("./image/code.ico"));
    //setFixedSize(734,554);//设置窗口不可拉伸
    ui->pushButton_language->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_head->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_shujujiegou->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_function->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_exit->setStyleSheet("QPushButton{background-color:black;color:white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );//按钮美化 黑底白字，选中变白，点击变蓝
    ui->pushButton_init->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_autocreate->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_test->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_cmd->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );

    //网络部分
    tcpSocket=new QTcpSocket;
    host="127.0.0.1";
    net="6666";
    connect(tcpSocket,&QTcpSocket::readyRead,this,&MainWindow::readmessage);
    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
    //利用线程中的阻塞来让程序延迟一秒。
    /*QMutex mutex;
    QWaitCondition sleep;
    mutex.lock();
    sleep.wait(&mutex, 1000);
    mutex.unlock();*/
    //init_knn();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_exit_clicked()
{
    exit(0);
}
void MainWindow::showauthor()
{
    a=new author;
    a->setWindowFlags(a->windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);
    a->show();

}

void MainWindow::on_pushButton_language_clicked()
{
    author a;
    a.cdexshow();
}

void MainWindow::on_pushButton_head_clicked()
{
    author a;
    a.headinfoshow();
}

void MainWindow::on_pushButton_shujujiegou_clicked()
{
    author a;
    a.shujushow();
}


void MainWindow::on_pushButton_function_clicked()
{
    author a;
    a.funcshow();
}

void MainWindow::on_pushButton_init_clicked()
{
    init=new initial;
    init->show();
}

void MainWindow::on_pushButton_autocreate_clicked()
{
    author a;
    a.createshow();
}

void MainWindow::on_pushButton_test_clicked()
{
    quest=new question;
    quest->show();

}
void MainWindow::openGitee()
{
    QDesktopServices::openUrl(QUrl(QLatin1String("https://gitee.com/onlyyyy/smartC/releases")));
}
void MainWindow::checkUpdate()
{
    newConnect();
    currentVersion();
}
void MainWindow::newConnect()
{
    blocksize=0;//初始化数据大小为0
    tcpSocket->abort();//取消了当前已经存在的连接，并且重置套接字
    //连接到指定主机的指定端口
    tcpSocket->connectToHost(host,net.toInt());
}
void MainWindow::readmessage()
{
    QDataStream in(tcpSocket);//数据流入套接字
    in.setVersion(QDataStream::Qt_5_5);
    //如果是刚开始接收数据，因为初始化大小为0
    if(blocksize==0)
    {
        //判断所接收的数据是否大于两个字节，也就是文件的大小信息的空间
        //如果是，则把文件大小信息保存到blocksize
        if(tcpSocket->bytesAvailable()<(int)sizeof(quint16))
            return;
        in>>blocksize;//发送和接收的数据流都是有自己的格式的，比如，自己定义的quint16 blocksize;

    }
    //如果没有得到全部的数据，则返回，继续接收数据
    if(tcpSocket->bytesAvailable()<blocksize)
        return;
    in>>message;
    qDebug()<<message;

}
void MainWindow::displayError(QAbstractSocket::SocketError)
{
    qDebug()<<"test";
    qDebug()<<tcpSocket->errorString();
}
void MainWindow::currentVersion()
{
    qDebug()<<message;
    QString t;
    t="当前版本为v1.3\n最新版本为:"+message+"\n需要打开官方网站吗？";
    if(message=="v1.0")
    {
        QMessageBox::about(NULL,"提示","当前已经是最新版本");

    }
    else
    {
        int ret;
        ret=QMessageBox::question(NULL,"检查更新",t);
        if(QMessageBox::Yes==ret)
        {
            openGitee();
        }
    }
}

void MainWindow::on_pushButton_cmd_clicked()
{
    CMD=new cmd;
    CMD->show();
}
void MainWindow::init_knn()//x/=sum,y/=sum,sum=0
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql_find,sql_update;//一个查找，一个修改
    QModelIndex indexx,indexy,indexsum;
    sql_find="select x,y,sum from knn where name='cdex'";
    model->setQuery(sql_find);
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    indexsum=model->index(0,2);
    float tempx,tempy,tempsum;
    tempx=indexx.data().toFloat();
    tempy=indexy.data().toFloat();
    tempsum=indexsum.data().toFloat();
    tempx=tempx/tempsum;
    tempy=tempy/tempsum;
    tempsum=0;
    QString tempxx,tempyy,tempsums;
    tempxx=QString::number(tempx);
    tempyy=QString::number(tempy);
    tempsums=QString::number(tempsum);
    sql_update="update knn set x='"+tempxx+"',y='"+tempyy+"',sum='"+tempsums+"' where name='cdex'";
    model->setQuery(sql_update);
}
