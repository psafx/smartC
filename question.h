﻿#ifndef QUESTION_H
#define QUESTION_H
#include"function.h"
#include <QMainWindow>
#pragma execution_character_set("utf-8")
namespace Ui {
class question;
}

class question : public QMainWindow
{
    Q_OBJECT

public:
    explicit question(QWidget *parent = 0);
    ~question();
    void rand();//生成随机的习题，保存题号至数组中
    void SetQuestion(int now);//出题程序
    void button_question();//根据按钮来出题的函数
    void store();//存放答案，一次处理
    void correctJ();//存放正确答案函数
    void judge();
    void init();//对于返回按钮的初始化设计
    void button_init();//对于按钮的特殊初始化

private slots:
    void on_pushButton_exit_clicked();

    void on_pushButton_enter_clicked();

    void on_pushButton_next_clicked();



    void on_pushButton_1_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_return_clicked();

    void on_pushButton_reason_clicked();

    void on_pushButton_info_clicked();
    void showcreate();
    void showshuju();
    void showfunc();
    void showcdex();
    void showhead();
private:
    Ui::question *ui;
    int questionnum[10];//数组存放所有的题号，由于题目数量只可能是5或者10，所以初始长度设为10即可
    int now;//目前做到的题目数
    QString section,diffculty,number;
    int Number;
    int ans[10];//目前的答案 1=A 2=B 3=C 4=D
    int correct[10];//正确答案
    int answ[10];//对错 1为正确 0为错误
    QString currentnum;//当前正在显示题号
};

#endif // QUESTION_H
