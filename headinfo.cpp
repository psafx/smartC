﻿#include "headinfo.h"
#include "ui_headinfo.h"
#include<QSqlQueryModel>
#include<QString>
#include"author.h"
#include<QIcon>
#include<QSettings>
#include<QProcess>
#include<QWaitCondition>
#include<QMutex>
#pragma execution_character_set("utf-8")
headinfo::headinfo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::headinfo)
{
    ui->setupUi(this);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->horizontalHeader()->setSectionsClickable(false);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setShowGrid(false);  // disable the table grid.
    ui->tableView->verticalHeader()->setDefaultSectionSize(25);  // set row height.
    ui->tableView->horizontalHeader()->setHighlightSections(false);
    ui->tableView->setFrameShape(QFrame::NoFrame);
    QPixmap pixmap = QPixmap("./image/head.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    init();
    ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));//设置退出按钮的图标
    connect(ui->actionC_cdex,SIGNAL(triggered(bool)),this,SLOT(menu_cdex()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(showshuju()));
    connect(ui->action_function,SIGNAL(triggered(bool)),this,SLOT(showfunc()));
    connect(ui->action_create,SIGNAL(triggered(bool)),this,SLOT(showcreate()));
    connect(ui->action_test,SIGNAL(triggered(bool)),this,SLOT(showtest()));
    setFixedSize(800,600);//设置窗口不可拉伸
    ui->actionC_cdex->setIcon(QIcon("./image/opencdex.ico"));
    ui->action_create->setIcon(QIcon("./image/create.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_function->setIcon(QIcon("./image/openfunc.ico"));
    ui->action_test->setIcon(QIcon("./image/exercise1.ico"));
    ui->pushButton_exit->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );//按钮美化 黑底白字，选中变白，点击变蓝
    //invite_judge();
    knn_head_init();
    QProcess *caller=new QProcess(parent);
    caller->start("F:\\smartC\\knn_head\\knn_head.exe");
    QMutex mutex;
    QWaitCondition sleep;
    mutex.lock();
    sleep.wait(&mutex, 1000);
    mutex.unlock();
    knn_head_judge();
    qDebug()<<"wtf";
}

headinfo::~headinfo()
{
    delete ui;
}

void headinfo::on_pushButton_exit_clicked()
{

   close();

}
void headinfo::init()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString a;
    a="select name as 名称 from head order by name";
    model->setQuery(a);
    ui->tableView->setModel(model);
}
void headinfo::Show()
{
    QSettings a("./data.ini",QSettings::IniFormat);
    QVariant currentData=ui->tableView->currentIndex().data();

    if(currentData=="iostream")
    {
        IOSTREAM();
        QString info=a.value("/head/iostream").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("iostream",INFO);
        a.endGroup();
    }
    else if(currentData=="stdio")
    {
        STDIO();
        QString info=a.value("/head/stdio").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("stdio",INFO);
        a.endGroup();
    }
    else if(currentData=="string")
    {
        STRING();
        QString info=a.value("/head/string").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("string",INFO);
        a.endGroup();
    }
    else if(currentData=="cstdlib")
    {
        CSTDLIB();
        QString info=a.value("/head/cstdlib").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("cstdlib",INFO);
        a.endGroup();
    }
    else if(currentData=="vector")
    {
        VECTOR();
        QString info=a.value("/head/vector").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("vector",INFO);
        a.endGroup();
    }
    else if(currentData=="cmath")
    {
        MATH();
        QString info=a.value("/head/cmath").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("cmath",INFO);
        a.endGroup();
    }
    else if(currentData=="iomanip")
    {
        IOMANIP();
        QString info=a.value("/head/iomanip").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("iomanip",INFO);
        a.endGroup();
    }
    else if(currentData=="map")
    {
        MAP();
        QString info=a.value("/head/map").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("map",INFO);
        a.endGroup();
    }
    else if(currentData=="cctype")
    {
        CCTYPE();
        QString info=a.value("/head/cctype").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("head");
        a.setValue("cctype",INFO);
        a.endGroup();
    }
}
void headinfo::IOSTREAM()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("iostream的几个成员如下:");
    ui->textEdit->append("1.istream输入流类型，提供输入操作.");
    ui->textEdit->append("2.ostream输出流类型，提供输出操作.");
    ui->textEdit->append("3.cin,一个istream对象，标准输入流，用来从标准输入读取数据.");
    ui->textEdit->append("4.cout，一个ostream对象，标准输出流，用来从标准输出写入数据.");
    ui->textEdit->append("5.cerr，一个ostream对象，标准错误流，输出程序错误信息与其他不正常逻辑的内容.");
    ui->textEdit->append("6.clog，一个ostream对象，标准错误流，关联到标准错误，默认存放在缓冲区.");
    ui->textEdit->append("7.>>运算符，用来从一个istream对象读取输入数据.");
    ui->textEdit->append("8.<<运算符，用来向一个ostream对象写入输出数据.");
    ui->textEdit->append("9.getline函数，从一个给定的istream对象中读取一行数据，写入一个给定的string对象中.");
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='iostream'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);


}

void headinfo::on_tableView_clicked(const QModelIndex &index)
{
    Show();
    knn_add();
}
void headinfo::find()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString a,b;
    b=ui->lineEdit_find->text();
    a="select name as 名称 from head where name like '%"+b+"%' order by name";
    model->setQuery(a);
    ui->tableView->setModel(model);
}
void headinfo::on_lineEdit_find_textChanged(const QString &arg1)
{
    find();
}
void headinfo::STDIO()
{
    /*
    ui->textEdit->setText("");

    ui->textEdit->append("stdio 就是指 standard input & output（标准输入输出）。");
    ui->textEdit->append("所以这个头文件是对C语言的标准输入输出支持。");
    ui->textEdit->append("它所支持的标准函数如下：");
    ui->textEdit->append("int getchar()//从标准输入设备读入一个字符");
    ui->textEdit->append("int putchar()//向标准输出设备写出一个字符");
    ui->textEdit->append("int scanf(char*format[,argument…])//从标准输入设备读入格式化后的数据");
    ui->textEdit->append("int printf(char*format[,argument…])//向标准输出设备输出格式化字符串");
    ui->textEdit->append("char gets(char*string)//从标准输入设备读入一个字符串");
    ui->textEdit->append("int puts(char*string)//向标准输出设备输出一个字符串");
    ui->textEdit->append("int sprintf(char*string,char*format[,…])//把格式化的数据写入某个字符串缓冲区");
    ui->textEdit->append("在C++中一般不使用这些函数。");

    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='stdio'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::STRING()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("string是C++标准库的一个重要的部分，主要用于字符串处理。可以使用输入输出流方式直接进行操作，也可以通过文件等手段进行操作。");
    ui->textEdit->append("声明和初始化方法：");
    ui->textEdit->append("string s;//声明一个string 对象");
    ui->textEdit->append("string ss[10];//声明一个string对象的数组");
    ui->textEdit->append("使用等号的初始化叫做拷贝初始化，不使用等号的初始化叫做直接初始化。");
    ui->textEdit->append("string s;//默认初始化，一个空字符串");
    ui->textEdit->append("string s2(s1);//s2是s1的副本");
    ui->textEdit->append("string s5=""hiya"";//拷贝初始化");
    ui->textEdit->append("string类还支持insert，append等函数。");
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='string'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);

}
void headinfo::CSTDLIB()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("cstdlib的本来面目是C语言中的库stdlib.h 。");
    ui->textEdit->append("比如： C语言中的stdlib.h在C++被重命名为cstdlib C语言中的ctype.h在C++中被重命名为cctype C语言中的stdio.h在C++中被重命名为cstdio ");
    ui->textEdit->append("C语言中的time.h在C++中被重命名为ctime。在C++中，C语言中的一些库被进行了重命名，去掉了.h并在库名前加c。");
    ui->textEdit->append("cstdlib（即：stdlib.h）中常用到的函数：");
    ui->textEdit->append("rand函数：用于产生随机数");
    ui->textEdit->append("srand函数：用于初始化随机数种子");
    ui->textEdit->append("system函数：用于DOS系统功能调用");
    ui->textEdit->append("exit函数：用于退出程序");
    ui->textEdit->append("qsort函数：快速排序");
    ui->textEdit->append("itoa、atoi、atof等一系列转换函数");
    ui->textEdit->append("malloc函数：（也可以用头文件malloc.h）动态分配内存");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='cstdlib'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::VECTOR()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("C++ vector类为内置数组提供了一种替代表示，与string类一样 vector 类是随标准 C++引入的标准库的一部分，使用时需包含头文件：");
    ui->textEdit->append("#include <vector>");
    ui->textEdit->append("vector使用总结：");
    ui->textEdit->append("1.vector的初始化：可以有五种方式,举例说明如下：");
    ui->textEdit->append("(1）vector<int> a(10); //定义了10个整型元素的向量（尖括号中为元素类型名，它可以是任何合法的数据类型），但没有给出初值，其值是不确定的。");
    ui->textEdit->append("（2）vector<int>a(10,1); //定义了10个整型元素的向量,且给出每个元素的初值为1");
    ui->textEdit->append("（3）vector<int>a(b); //用b向量来创建a向量，整体复制性赋值");
    ui->textEdit->append("（4）vector<int>a(b.begin(),b.begin+3); //定义了a值为b中第0个到第2个（共3个）元素");
    ui->textEdit->append("（5）intb[7]={1,2,3,4,5,9,8};vector<int> a(b,b+7); //从数组中获得初值");
    ui->textEdit->append("2.vector对象的几个重要操作，举例说明如下：");
    ui->textEdit->append("（1）a.assign(b.begin(), b.begin()+3);//b为向量，将b的0~2个元素构成的向量赋给a");
    ui->textEdit->append("（2）a.assign(4,2);//是a只含4个元素，且每个元素为2");
    ui->textEdit->append("（3）a.back();//返回a的最后一个元素");
    ui->textEdit->append("（4）a.front();//返回a的第一个元素");
    ui->textEdit->append("（5）a[i]; //返回a的第i个元素，当且仅当a[i]存在");
    ui->textEdit->append("（6）a.clear();//清空a中的元素");
    ui->textEdit->append("（7）a.empty();//判断a是否为空，空则返回ture,不空则返回false");
    ui->textEdit->append("（8）a.pop_back();//删除a向量的最后一个元素");
    ui->textEdit->append("（9）a.erase(a.begin()+1,a.begin()+3);//删除a中第1个（从第0个算起）到第2个元素，也就是说删除的元素从a.begin()+1算起（包括它）一直到a.begin()+3（不包括它）");
    ui->textEdit->append("（10）a.push_back(5);//在a的最后一个向量后插入一个元素，其值为5");
    ui->textEdit->append("（11）a.insert(a.begin()+1,5);//在a的第1个元素（从第0个算起）的位置插入数值5，如a为1,2,3,4，插入元素后为1,5,2,3,4");
    ui->textEdit->append("（12）a.insert(a.begin()+1,3,5);//在a的第1个元素（从第0个算起）的位置插入3个数，其值都为5");
    ui->textEdit->append("（13）a.insert(a.begin()+1,b+3,b+6);//b为数组，在a的第1个元素（从第0个算起）的位置插入b的第3个元素到第5个元素（不包括b+6），如b为1,2,3,4,5,9,8，插入元素后为1,4,5,9,2,3,4,5,9,8");
    ui->textEdit->append("（14）a.size();//返回a中元素的个数；");
    ui->textEdit->append("（15）a.capacity();//返回a在内存中总共可以容纳的元素个数");
    ui->textEdit->append("（16）a.rezize(10);//将a的现有元素个数调至10个，多则删，少则补，其值随机");

    ui->textEdit->append("（17）a.rezize(10,2);//将a的现有元素个数调至10个，多则删，少则补，其值为2");
    ui->textEdit->append("（18）a.reserve(100);//将a的容量（capacity）扩充至100 ");
    ui->textEdit->append("，也就是说现在测试a.capacity();的时候返回值是100.这种操作只有在需要给a添加大量数据的时候才 显得有意义，因为这将避免内存多次容量扩充操作（当a的容量不足时电脑会自动扩容，当然这必然降低性能）");
    ui->textEdit->append("（19）a.swap(b);//b为向量，将a中的元素和b中的元素进行整体性交换");
    ui->textEdit->append("（20）a==b; //b为向量，向量的比较操作还有!=,>=,<=,>,<");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);

    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='vector'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::MATH()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("“math.h”是C语言中数学函数库，包含我们常用的一些数学计算上会使用到的函数。C++中有对应相同作用的头文件“cmath”，当然C++中两个头文件都可以使用，C++向C兼容。");
    ui->textEdit->append("常会使用的函数主要有以下几个：");
    ui->textEdit->append("double log (double); 　　　　 以e为底的对数");
    ui->textEdit->append("double log10 (double);　　　　以10为底的对数 ");
    ui->textEdit->append("double pow(double x,double y);计算x的y次幂 ");
    ui->textEdit->append("double exp (double);　　　　　求取自然数e的幂 ");
    ui->textEdit->append("double sqrt (double);　　　　 开平方 ");
    ui->textEdit->append("int 　 abs(int i); 　　　　　 求整型的绝对值 ");
    ui->textEdit->append("double fabs (double);　　　　 求实型的绝对值");
    ui->textEdit->append("另外还包含了一系列的三角函数接口。");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='cmath'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::IOMANIP()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("iomanip在c++程序里面经常见到下面的头文件#include <iomanip>，io代表输入输出，manip是manipulator（操纵器）的缩写(在c++上只能通过输入缩写才有效。）");
    ui->textEdit->append("主要是对cin,cout之类的一些操纵运算子，比如setfill,setw,setbase,setprecision等等。它是I/O流控制头文件,就像C里面的格式化输出一样");
    ui->textEdit->append(".以下是一些常见的控制函数:");
    ui->textEdit->append("dec 置基数为10 相当于""%d""");
    ui->textEdit->append("hex 置基数为16 相当于""%X""");
    ui->textEdit->append("setfill( 'c' ) 设填充字符为c");
    ui->textEdit->append("setprecision( n ) 设显示有效数字为n位");
    ui->textEdit->append("setw( n ) 设域宽为n个字符");
    ui->textEdit->append("这个控制符的意思是保证输出宽度为n。如：");
    ui->textEdit->append("cout << setw( 3 ) << 1 << setw( 3 ) << 10 << setw( 3 ) << 100 << endl; 输出结果为");

    ui->textEdit->append("_ _1_10100 （默认是右对齐）当输出长度大于3时(<<1000)，setw(3)不起作用。");
    ui->textEdit->append("setbase(int n) : 将数字转换为 n 进制.");
    ui->textEdit->append("如 cout<<setbase(8)<<setw(5)<<255<<endl;");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='iomanip'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::MAP()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("Map是c++的一个标准容器，她提供了很好一对一的关系，在一些程序中建立一个map可以起到事半功倍的效果，总结了一些map基本简单实用的操作");
    ui->textEdit->append("1. map最基本的构造函数；");
    ui->textEdit->append("map<string , int >mapstring;         map<int ,string >mapint;");
    ui->textEdit->append("map<sring, char>mapstring;         map< char ,string>mapchar;");
    ui->textEdit->append("map<char ,int>mapchar;            map<int ,char >mapint；");
    ui->textEdit->append("2. map添加数据；");
    ui->textEdit->append("map<int ,string> maplive;  ");
    ui->textEdit->append("(1).maplive.insert(pair<int,string>(102,""aclive""));");
    ui->textEdit->append("(2).maplive.insert(map<int,string>::value_type(321,""hai""));");
    ui->textEdit->append("(3), maplive[112]=""April"";//map中最简单最常用的插入添加");
    ui->textEdit->append("3，map中元素的查找：");
    ui->textEdit->append("find()函数返回一个迭代器指向键值为key的元素，如果没找到就返回指向map尾部的迭代器。    ");
    ui->textEdit->append("map<int ,string >::iterator l_it;; ");
    ui->textEdit->append("l_it=maplive.find(112);");
    ui->textEdit->append("if(l_it==maplive.end())");
    ui->textEdit->append("cout<<""we do not find 112""<<endl;");
    ui->textEdit->append("else cout<<""wo find 112""<<endl;");
    ui->textEdit->append("4,map中元素的删除：");
    ui->textEdit->append("如果删除112；");
    ui->textEdit->append("map<int ,string >::iterator l_it;;");
    ui->textEdit->append("l_it=maplive.find(112);");
    ui->textEdit->append("if(l_it==maplive.end())");
    ui->textEdit->append("cout<<""we do not find 112""<<endl;");
    ui->textEdit->append("else  maplive.erase(l_it);  //delete 112;");
    ui->textEdit->append("5,map中 swap的用法：");
    ui->textEdit->append("Map中的swap不是一个容器中的元素交换，而是两个容器交换");
    ui->textEdit->append("6.map的sort问题：");
    ui->textEdit->append("Map中的元素是自动按key升序排序,所以不能对map用sort函数");
    ui->textEdit->append("7,map的基本操作函数：");
    ui->textEdit->append("C++ Maps是一种关联式容器，包含“关键字/值”对");
    ui->textEdit->append("begin()          返回指向map头部的迭代器");
    ui->textEdit->append("clear(）         删除所有元素");
    ui->textEdit->append("count()          返回指定元素出现的次数");
    ui->textEdit->append("empty()          如果map为空则返回true");
    ui->textEdit->append("end()            返回指向map末尾的迭代器");
    ui->textEdit->append("equal_range()    返回特殊条目的迭代器对");
    ui->textEdit->append("erase()          删除一个元素");
    ui->textEdit->append("find()           查找一个元素");
    ui->textEdit->append("get_allocator()  返回map的配置器");
    ui->textEdit->append("insert()         插入元素");
    ui->textEdit->append("key_comp()       返回比较元素key的函数");
    ui->textEdit->append("lower_bound()    返回键值>=给定元素的第一个位置");
    ui->textEdit->append("max_size()       返回可以容纳的最大元素个数");
    ui->textEdit->append("rbegin()         返回一个指向map尾部的逆向迭代器");
    ui->textEdit->append("rend()           返回一个指向map头部的逆向迭代器");
    ui->textEdit->append("size()           返回map中元素的个数");
    ui->textEdit->append("swap()            交换两个map");
    ui->textEdit->append("upper_bound()     返回键值>给定元素的第一个位置");
    ui->textEdit->append("value_comp()      返回比较元素value的函数");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='map'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::CCTYPE()
{
    /*
    ui->textEdit->setText("");
    ui->textEdit->append("ctype.h是C标准函数库中的头文件，定义了一批C语言字符分类函数（C character classification functions），用于测试字符是否属于特定的字符类别，如字母字符、控制字符等等。既支持单字节（Byte)字符，也支持宽字符。");
    ui->textEdit->append("cctype中通常包括一些常用函数的判断，如某个字符是否为大写，用isupper()如果参数是大写字母，函数返回true, ");
    ui->textEdit->append("还有像isalnum(),如果参数是字母数字，即字母或者数字，函数返回true.");
    ui->textEdit->append("函数名称   返回值");
    ui->textEdit->append("isalnum()  如果参数是字母数字，即字母或者数字，函数返回true");
    ui->textEdit->append("isalpha()   如果参数是字母，函数返回true");
    ui->textEdit->append("isblank()   如果参数是水平制表符或空格，函数返回true");
    ui->textEdit->append("iscntrl()     如果参数是控制字符，函数返回true");
    ui->textEdit->append("isdigit()     如果参数是数字（0－9），函数返回true");
    ui->textEdit->append("isgraph()   如果参数是除空格之外的打印字符，函数返回true");
    ui->textEdit->append("islower()    如果参数是小写字母，函数返回true");
    ui->textEdit->append("isprint()      如果参数是打印字符（包括空格），函数返回true");
    ui->textEdit->append("ispunct()    如果参数是标点符号，函数返回true");
    ui->textEdit->append("isspace()   如果参数是标准空白字符，如空格、换行符、水平或垂直制表符，函数返回true");
    ui->textEdit->append("isupper()   如果参数是大写字母，函数返回true");
    ui->textEdit->append("isxdigit()     如果参数是十六进制数字，即0－9、a－f、A－F，函数返回true");
    ui->textEdit->append("tolower()    如果参数是大写字符，返回其小写，否则返回该参数");
    ui->textEdit->append("toupper()   如果参数是小写字符，返回其大写，否则返回该参数");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    */
    QSqlQueryModel *model1=new QSqlQueryModel;
    QString sql1="select code from head where name='cctype'";
    model1->setQuery(sql1);
    QModelIndex index1=model1->index(0,0);
    QString word=index1.data().toString();
    ui->textEdit->setText(word);
}
void headinfo::menu_cdex()
{
    author a;

    a.cdexshow();
    close();
}
void headinfo::showshuju()
{
    author a;
    a.shujushow();
    close();
}
void headinfo::showfunc()
{
    author a;
    a.funcshow();
    close();
}
void headinfo::invite_set()
{
    int INFO;

    QSettings invi("./data.ini",QSettings::IniFormat);
    QString info=invi.value("/head/stdio").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="stdio";
    }
    info=invi.value("/head/string").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="string";
    }
    info=invi.value("/head/iostream").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="iostream";
    }
    info=invi.value("/head/vector").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="vector";
    }
    info=invi.value("/head/cstdlib").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="cstdlib";
    }
    info=invi.value("/head/map").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="map";
    }
    info=invi.value("/head/cctype").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="cctype";
    }
    info=invi.value("/head/iomanip").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="iomanip";
    }
    info=invi.value("/head/cmath").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="cmath";
    }
}
void headinfo::invite()
{
    if(maxS=="stdio")
    {
        STDIO();
    }
    else if(maxS=="string")
    {
        STRING();
    }
    else if(maxS=="iostream")
    {
        IOSTREAM();
    }
    else if(maxS=="vector")
    {
        VECTOR();
    }
    else if(maxS=="cstdlib")
    {
        CSTDLIB();
    }
    else if(maxS=="map")
    {
        MAP();
    }
    else if(maxS=="cctype")
    {
        CCTYPE();
    }
    else if(maxS=="iomanip")
    {
        IOMANIP();
    }
    else if(maxS=="cmath")
    {
        MATH();
    }
}
void headinfo::invite_judge()
{
    invite_set();
    if(max!=0)
    {
       invite();
       QString a1;
       a1="你最常查看的搜索项是："+maxS+"\n现在开始自动推送";
       QMessageBox::about(NULL, "智能推荐", a1);

    }
}
void headinfo::showcreate()
{
    hide();
    author a;
    a.createshow();
}
void headinfo::showtest()
{
    hide();
    author a;
    a.questionshow();
}
void headinfo::knn_add()
{
    QVariant data=ui->tableView->currentIndex().data();
    QString current=data.toString();//目前鼠标点击的项
    float cdex_x,cdex_y;//需要插入的坐标值
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql_origin,sql_direction,sql_update;//先查需要插的值是多少，再写新的语句插入
    sql_origin="select x,y from head where name='"+current+"'";
    model->setQuery(sql_origin);//结果为一行两列的坐标值.
    QModelIndex indexx,indexy,indexsum;
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    cdex_x=indexx.data().toFloat();
    cdex_y=indexy.data().toFloat();
   //qDebug()<<cdex_x;
    //qDebug()<<cdex_y;
    float currentx,currenty,currentsum;
    float final_x,final_y,final_sum;//最终值
    sql_direction="select x,y,sum from knn where name='head'";
    model->setQuery(sql_direction);
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    indexsum=model->index(0,2);
    currentx=indexx.data().toFloat();
    currenty=indexy.data().toFloat();
    currentsum=indexsum.data().toFloat();
    final_x=currentx+cdex_x;
    final_y=currenty+cdex_y;
    final_sum=currentsum+1;//点击一次，就加一次，因为记录的就是统计的次数
    QString a,b,c;
    a=QString::number(final_x);
    b=QString::number(final_y);
    c=QString::number(final_sum);
    sql_update="update knn set x='"+a+"',y='"+b+"',sum='"+c+"' where name='head'";
    model->setQuery(sql_update);
}
void headinfo::knn_head_init()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql_find,sql_update;//一个查找，一个修改
    QModelIndex indexx,indexy,indexsum;
    sql_find="select x,y,sum from knn where name='head'";
    model->setQuery(sql_find);
    indexx=model->index(0,0);//x
    indexy=model->index(0,1);//y
    indexsum=model->index(0,2);//sum
    float tempx,tempy,tempsum;
    tempx=indexx.data().toFloat();
    tempy=indexy.data().toFloat();
    tempsum=indexsum.data().toFloat();
    int ifzero=indexsum.data().toInt();//判断sum是否为0!!不能x/0否则数据库会down掉
    if(ifzero!=0)
    {
     tempx=tempx/tempsum;
    tempy=tempy/tempsum;
     tempsum=0;
     QString tempxx,tempyy,tempsums;
   tempxx=QString::number(tempx);
      tempyy=QString::number(tempy);
     tempsums=QString::number(tempsum);
      sql_update="update knn set x='"+tempxx+"',y='"+tempyy+"',sum='"+tempsums+"' where name='head'";
       model->setQuery(sql_update);
     }
}
void headinfo::knn_head_judge()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    model->setQuery("select result from knn where name='head'");
    QModelIndex index;
    index=model->index(0,0);
    char result;
    QString  result1;
    result1=index.data().toString();
    if(result1=="d")
    {
        result='d';
    }
    else if(result1=="i")
    {
        result='i';
    }
    else if(result1=="y")
    {
        result='y';
    }
    else if(result1=="m")
    {
        result='m';
    }
    else if(result1=="a")
    {
        result='a';
    }
    else if(result1=="r")
    {
        result='r';
    }
    else if(result1=="v")
    {
        result='v';
    }
    else if(result1=="b")
    {
        result='b';
    }
    else if(result1=="p")
    {
        result='p';
    }

    //result=result1.toLatin1();//QChar 转char
    qDebug()<<"??"<<result1;
    switch (result)
    {
    case 'd':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐stdio");
        STDIO();
        break;
    case 'i':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐iostream");
        IOSTREAM();
        break;
    case 'y':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐cctype");
        CCTYPE();
        break;
    case 'm':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐cmath");
        MATH();
        break;
    case 'a':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐map");
        MAP();
        break;
    case 'v':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐vector");
        VECTOR();
        break;
    case 'r':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐string");
        STRING();
        break;
    case 'b':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐cstdlib");
        CSTDLIB();
        break;
    case 'p':
        QMessageBox::about(NULL,"智能推荐","根据您的使用习惯向您推荐iomanip");
        IOMANIP();
        break;

    }
}
