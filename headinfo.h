﻿#ifndef HEADINFO_H
#define HEADINFO_H
#pragma execution_character_set("utf-8")
#include <QMainWindow>
#include<QMessageBox>
namespace Ui {
class headinfo;
}

class headinfo : public QMainWindow
{
    Q_OBJECT

public:
    explicit headinfo(QWidget *parent = 0);
    ~headinfo();
    void knn_add();//每次点击就加坐标值
    void knn_head_init();
    void knn_head_judge();//判断推荐哪个搜索项
private slots:
    void on_pushButton_exit_clicked();
    void init();
    void Show();
    void IOSTREAM();
    void on_tableView_clicked(const QModelIndex &index);
    void find();
    void on_lineEdit_find_textChanged(const QString &arg1);
    void STDIO();
    void STRING();
    void CSTDLIB();
    void VECTOR();
    void MATH();
    void IOMANIP();
    void MAP();
    void CCTYPE();
    void menu_cdex();//实现用菜单切换到cdex的函数

    void invite_set();
    void invite();
    void invite_judge();
    void showcreate();
    void showshuju();
    void showfunc();
    void showtest();
private:
    Ui::headinfo *ui;
    QString maxS;
    int max=0;
};

#endif // HEADINFO_H
