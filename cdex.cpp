﻿#include "cdex.h"
#include "ui_cdex.h"
#include<QSqlQueryModel>
#include<QString>
#include<QSettings>
#include"author.h"
#include<QMessageBox>
#include<knn.h>
#pragma execution_character_set("utf-8")//设置中文= =

cdex::cdex(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::cdex)
{
    ui->setupUi(this);

    connect(ui->action_head,SIGNAL(triggered(bool)),this,SLOT(showhead()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(showshuju()));
    connect(ui->action_func,SIGNAL(triggered(bool)),this,SLOT(showfunc()));
    connect(ui->action_create,SIGNAL(triggered(bool)),this,SLOT(showcreate()));
    connect(ui->action_test,SIGNAL(triggered(bool)),this,SLOT(showtest()));
   /*
    ui->tableView->setColumnWidth(1,102);//设置列宽不可变
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);//只读
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
    model->setItem(0,0,new QStandardItem("if"));

    */
    //ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);//整行选中
    ui->tableView->verticalHeader()->hide();
    ui->tableView->horizontalHeader()->setSectionsClickable(false);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setShowGrid(false);  // disable the table grid.
    ui->tableView->verticalHeader()->setDefaultSectionSize(25);  // set row height.
    ui->tableView->horizontalHeader()->setHighlightSections(false);
    ui->tableView->setFrameShape(QFrame::NoFrame);
    //ui->tableView->setItemDelegate(new NoFocusFrameDelegate());
    QPixmap pixmap = QPixmap("./image/cdex.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));//设置退出按钮的图标
    setFixedSize(1032,647);//设置窗口不可拉伸
    init();
    ui->action_test->setIcon(QIcon("./image/exercise1.ico"));
    ui->action_head->setIcon(QIcon("./image/openhead.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_func->setIcon(QIcon("./image/openfunc.ico"));
    ui->action_create->setIcon(QIcon("./image/create.ico"));
    ui->pushButton_exit->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );//按钮美化 黑底白字，选中变白，点击变蓝


   //invite_judge();
    knn_cdex_init();
    knn_show();
}

cdex::~cdex()
{
    delete ui;
}

void cdex::on_pushButton_exit_clicked()
{
    hide();


}

void cdex::on_pushButton_find_clicked()//实现查找某个用法的功能
{
   QSqlQueryModel *model=new QSqlQueryModel;
   QString a,b;
   b=ui->lineEdit_find->text();
   a="select name as '名称' from yufa where name LIKE '%"+b+"%' order by name";
   model->setQuery(a);
   ui->tableView->setModel(model);
}
void cdex::init()//初始化显示所有的搜索项列表
{
    QSqlQueryModel *model=new QSqlQueryModel;
    model->setQuery("select name as '名称' from yufa order by name");
    ui->tableView->setModel(model);

}
void cdex::WHILE()
{
    /*ui->textEdit->setText("");
    ui->textEdit->append("while语句的一般形式如下：");
    ui->textEdit->append("while (表达式) 语句");
    ui->textEdit->append("其作用是：当指定的条件为真(表达式为非0)时，执行while语句中的内嵌语句。");
    ui->textEdit->append("其特点是：先判断表达式，后执行语句。while循环称为当型循环。");
    ui->textEdit->append("while会判断表达式是否等于0,否则就会不断地执行循环语句");
    ui->textEdit->append("比如while(n--)是会不断的判断n-1之后的值是否等于0,不等于0就会不断的进行循环");
    ui->textEdit->append("while的两种特殊用法如下:");
    ui->textEdit->append("while(1)是执行一个相当于死循环的语句,循环体中除非有能跳出循环的语句,否则会不断地循环");
    ui->textEdit->append("while(cin>>x)是要求用户不断的输入变量的值,但却没有给跳出循环的方式.");
    ui->textEdit->append("在Windows下通过组合件Ctrl+Z跳出这样的while循环");
    ui->textEdit_code->setText("");
    ui->textEdit_code->append("int i=0");
    ui->textEdit_code->append("while(i<10)");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("i++;");
    ui->textEdit_code->append("cout<<i<,endl;");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("//这个while循环类似于for循环,");
    ui->textEdit_code->append("将i从0加到9并且每次都将它显示在屏幕中");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='while'";
    model1->setQuery(sql1);
    //ui->listView_word->setModel(model1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    //118-122 将数据库搜索得到的结果保存在QString中，再从textEdit中显示
    QString sql2="select code from yufa where name='while'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}
void cdex::Show()
{

    QVariant currentData =ui->tableView->currentIndex().data();
    QSettings a("./data.ini",QSettings::IniFormat);
    if(currentData=="if")
    {


        QString info=a.value("/cdex/if").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("if",INFO);
        a.endGroup();
        IF();


    }
    else if(currentData=="while")
    {
        WHILE();

        QString info=a.value("/cdex/while").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("while",INFO);
        a.endGroup();
    }
    else if(currentData=="for")
    {
        FOR();


        QString info=a.value("/cdex/for").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("for",INFO);
        a.endGroup();
    }
    else if(currentData=="switch")
    {
        SWITCH();


        QString info=a.value("/cdex/switch").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("switch",INFO);
        a.endGroup();
    }

    else if(currentData=="struct")
    {
        STRUCT();


        QString info=a.value("/cdex/struct").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("struct",INFO);
        a.endGroup();
    }
    else if(currentData=="namespace")
    {
        NAMESPACE();


        QString info=a.value("/cdex/namespace").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("namespace",INFO);
        a.endGroup();
    }
    else if(currentData=="new")
    {
        NEW();


        QString info=a.value("/cdex/new").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("new",INFO);
        a.endGroup();
    }
    else if(currentData=="enum")
    {
        ENUM();


        QString info=a.value("/cdex/enum").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("enum",INFO);
        a.endGroup();
    }
    else if(currentData=="union")
    {
        UNION();


        QString info=a.value("/cdex/union").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("union",INFO);
        a.endGroup();
    }
    else if(currentData=="array")
    {
        ARRAY();


        QString info=a.value("/cdex/array").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("array",INFO);
        a.endGroup();
    }

    else if(currentData=="const")
    {
        CONST();


        QString info=a.value("/cdex/const").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("const",INFO);
        a.endGroup();
    }
    else if(currentData=="static")
    {
        STATIC();

        QString info=a.value("/cdex/static").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("static",INFO);
        a.endGroup();
    }
    else if(currentData=="define")
    {
        DEFINE();


        QString info=a.value("/cdex/define").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("define",INFO);
        a.endGroup();
    }
    else if(currentData=="pointer")
    {
        POINTER();


        QString info=a.value("/cdex/pointer").toString();
        int INFO;
        INFO=info.toInt();
        INFO++;
        a.beginGroup("cdex");
        a.setValue("pointer",INFO);
        a.endGroup();
    }
}


void cdex::FOR()
{
    /*ui->textEdit->setText("");//相当于清空当前textedit所有的内容
    ui->textEdit->append("for语句的一般形式如下：");
    ui->textEdit->append("for(表达式1; 表达式2; 表达式3)  语句");
    ui->textEdit->append("括号内的变量为局部变量,只在这个for循环内部使用");
    ui->textEdit->append("每个语句之间用分号间隔");
    ui->textEdit->append("如果循环体只有一行语句,可以省略大括号,不然都需要写在大括号以内");
    ui->textEdit->append("不过为了代码可读性,尽量每个for循环都用大括号修饰");
    ui->textEdit_code->setText("");
    ui->textEdit_code->append("int sum=0");
    ui->textEdit_code->append("for(int i=1;i<=100;i++)");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("  sum+=i;");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("cout<<sum<<endl;");
    ui->textEdit_code->append("//这是将整型变量sum从1一直加到100");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='for'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='for'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}

void cdex::on_tableView_clicked(const QModelIndex &index)
{
    Show();
    knn_cdex();
   // knn_show();//不该写在这里
}

void cdex::on_lineEdit_find_textChanged(const QString &arg1)//模糊查询在用户每次输入进去的时候都执行一次
{
    on_pushButton_find_clicked();
}
void cdex::IF()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("if语句的一般形式如下:");
    ui->textEdit->append("if(表达式)");
    ui->textEdit->append("如果判断为真,则执行if语句的内容");
    ui->textEdit->append("如果判断为假,则执行else语句的内容");
    ui->textEdit->append("当多个判断语句并存的时候,需要通过if,else if,else来实现");
    ui->textEdit->append("如果语句超过一行,则需要用大括号来进行修饰.");

    ui->textEdit_code->append("if(a==0)");
    ui->textEdit_code->append("  cout<<a+1<<endl;");
    ui->textEdit_code->append("else if(a==1)");
    ui->textEdit_code->append("  cout<<a+2<<endl;");
    ui->textEdit_code->append("else");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("  a=0;");
    ui->textEdit_code->append("  cout<<a<<endl;");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("//这是判断变量a的值,如果等于0则输出1,等于1则输出3,其他情况则置零.");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);
    ui->textEdit_code ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/

    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='if'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='if'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::SWITCH()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("switch语句的一般形式如下:");
    ui->textEdit->append("switch(字符或者数字)");
    ui->textEdit->append("case A:");
    ui->textEdit->append("case B:");
    ui->textEdit->append("switch是C++的开关语句,根据不同的情况给出相应的语句去执行");
    ui->textEdit->append("每个case最后都应该加上break;来跳出循环,不然switch会一直执行下去.");
    ui->textEdit->append("switch的特殊用法:");
    ui->textEdit->append("case 1:");
    ui->textEdit->append("case 2:");
    ui->textEdit->append("cout<<""helloworld""<<endl;");
    ui->textEdit->append("将两个case并列,意思是值等于1和2的时候都会执行如下的语句");
    ui->textEdit_code->append("cin>>i;");
    ui->textEdit_code->append("switch(i)");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("  case 1:");
    ui->textEdit_code->append("    i=0;");
    ui->textEdit_code->append("    break;");
    ui->textEdit_code->append("  case 0:");
    ui->textEdit_code->append("    i=1");
    ui->textEdit_code->append("    break;");
    ui->textEdit_code->append("  default:");
    ui->textEdit_code->append("    i=100");
    ui->textEdit_code->append("    break;");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("//判断i的值,0和1的时候值互换,否则就等于100");
    ui->textEdit_code->append("default类似于if语句里面的else用法");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='switch'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='switch'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::CLASS()
{
    showfunc();


}
void cdex::STRUCT()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("结构体是C语言中的一种构造类型,在C++中更多的使用类来替代结构体.");
    ui->textEdit->append("C语言的结构体不能包含函数,而C++的结构体可以");
    ui->textEdit->append("结构体更多的时候是将相关的元素放在结构体中,实现数据库表的功能,便于归类和调用.");
    ui->textEdit->append("结构体声明如下:");
    ui->textEdit->append("struct 结构体名称");
    ui->textEdit->append("{");
    ui->textEdit->append("变量声明");
    ui->textEdit->append("};");*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='struct'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='struct'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}
void cdex::NAMESPACE()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("namespace是为了防止命名发生冲突.");
    ui->textEdit->append("大型程序往往会使用多个独立开发的库,这些库又会定义大量的全局名字,如类、函数和模板等。");
    ui->textEdit->append("当应用程序用到多个供应商提供的库时，不可避免地会发生某些名字相互冲突的情况。");
    ui->textEdit->append("多个库将名字放置在全局命名空间中将引发命名空间污染(namespace pollution)。");
    ui->textEdit->append("命名空间(namespace)为防止名字冲突提供了更加可控的机制。命名空间分割了全局命名空间，");
    ui->textEdit->append("其中每个命名空间是一个作用域。通过在某个命名空间中定义库的名字，库的作者以及用户可以避免全局名字固有的限制。");
    ui->textEdit->append("命名空间定义：一个命名空间的定义包含两部分：首先是关键字namespace，随后是命名空间的名字。");
    ui->textEdit->append("namespace的三种定义方式:");
    ui->textEdit->append("using namespace std;//这种方式适合初学者能比较简便的定义命名空间");
    ui->textEdit->append("using std::cin;//这种方式只是把cin这个名称定义为标准命名空间,不会影响到其他的名称");
    ui->textEdit->append("//可以不需要引入整个std,能够让程序占用的资源更小");
    ui->textEdit->append("//最后一种方法");
    ui->textEdit->append("std::cin");
    ui->textEdit->append("这个方式是需要每次使用cin的时候都声明是标准命名空间,比较的繁琐,但在特殊情况下也有它的用武之地。");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='namespace'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='namespace'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::NEW()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("new操作符在C++中的用途是给指针分配内存空间。");
    ui->textEdit->append("例如int *p=new int.//这段代码就是为整型指针p分配一块内存空间。");
    ui->textEdit->append("而要删除这个空间，则需要如下的代码：");
    ui->textEdit->append("delete p;//这样就归还了之前分配的空间。");
    ui->textEdit->append("使用new来创建一维动态数组：");
    ui->textEdit->append("int *p=new int[N];//这里的N是用户输入的数组长度");
    ui->textEdit->append("这样就为指针p分配了一块连续的内存空间N，作为一个动态数组的用法。");
    ui->textEdit->append("而要归还这样的空间，代码就稍显特殊了：");
    ui->textEdit->append("delete[] p;//注意方括号的位置");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='new'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='new'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}
void cdex::ENUM()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("枚举enum的出现，主要是为了解决一些特定属性的赋值，变量取值仅在一定有限范围内的问题。");
    ui->textEdit->append("例如一年只有十二个月取值，一个星期只有七天情况，人的性别只有男女两种等。");
    ui->textEdit->append("这些属性如果简简单单用int类型变量定义就有失妥当，能取的值远远超出可取值范围。所以我们希望给这些属性变量，定义一个有限取值范围。");
    ui->textEdit->append("枚举即将变量的值一一列举出来，变量只限于列举出来的值的范围内取值。");
    ui->textEdit->append("枚举定义举例如下：");
    ui->textEdit->append("enum color {red=1 , green =5, blue=10} paint;");
    ui->textEdit->append("color paint = red;");
    ui->textEdit->append("enum color paint = red; ");
    ui->textEdit->append("");
    ui->textEdit_code->append("enum weekday {sun=1,mon,tue,wed,thu,fri,sat} day; //从1开始");
    ui->textEdit_code->append("//这样每天都会从1-7依次进行赋值。");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='enum'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='enum'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}
void cdex::UNION()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("共用体使用覆盖技术，几个变量相互覆盖，从而使几个不同变量共占同一段内存的结构，成为共同体类型的结构。");
    ui->textEdit->append("共同体的定义类似结构体，不过共同体的所有成员都在同一段内存中存放，起始地址一样，并且同一时刻只能使用其中的一个成员变量。");
    ui->textEdit->append("声明共用体的一般形式为：");
    ui->textEdit->append("union 共用体类型名");
    ui->textEdit->append("{");
    ui->textEdit->append("  成员列表");
    ui->textEdit->append("}");
    ui->textEdit->append("结构体变量所占长度是各成员占的内存长度之和。每个成员分别占有自己的内存单元。共用体变量所占的内存长度等于最长的成员的长度。");
    ui->textEdit->append("共用体的使用");
    ui->textEdit->append("1.不能引用共用体变量，而只能引用共用体变量中的成员。");
    ui->textEdit->append("2.使用共用体变量的目的是希望通过统一内存段存放几种不同类型的数据。");
    ui->textEdit->append("3.不能对共用体变量名赋值，不能企图引用变量名来得到一个值；不能在定义共用体变量时对它初始化，不能用共用体变量名作为函数参数。");
    ui->textEdit_code->append("#include<iostream>");
    ui->textEdit_code->append("using namespace std;");
    ui->textEdit_code->append("int main(){");
    ui->textEdit_code->append("union data{");
    ui->textEdit_code->append("char a;");
    ui->textEdit_code->append("char b;");
    ui->textEdit_code->append("};");
    ui->textEdit_code->append("data q;");
    ui->textEdit_code->append("q.a='q';");
    ui->textEdit_code->append("cout<<q.a<<endl;");
    ui->textEdit_code->append("q.b='m';");
    ui->textEdit_code->append("cout<<q.b<<endl;");
    ui->textEdit_code->append("return 0;");
    ui->textEdit_code->append("运行结果为：");
    ui->textEdit_code->append("q");
    ui->textEdit_code->append("m");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='union'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='union'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::ARRAY()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("在C++中， 数组属于构造数据类型。一个数组可以分解为多个数组元素，这些数组元素可以是基本数据类型或是构造类型。");
    ui->textEdit->append("因此按数组元素的类型不同，数组又可分为数值数组、字符数组、指针数组、结构数组等各种类别。");
    ui->textEdit->append("数组就是一次性定义相同数据类型的一组变量数组定义。");
    ui->textEdit->append("int a[10]; 说明整型数组a，有10个元素。若要表示第10个元素，则使用a[9]。第一个则是a[0]。");
    ui->textEdit->append("float b[10],c[20]; 说明实型数组b，有10个元素，实型数组c，有20个元素。");
    ui->textEdit->append("char ch[20]; 说明字符数组ch，有20个元素。");
    ui->textEdit->append("特点如下：");
    ui->textEdit->append("1.数组是相同数据类型的元素的集合。");
    ui->textEdit->append("2.数组中的各元素的存储是有先后顺序的，它们在内存中按照这个先后顺序连续存放在一起。");
    ui->textEdit->append("3.数组元素用整个数组的名字和它自己在数组中的顺序位置来表示。例如，a[0]表示名字为a的数组中的第一个元素，a[1]代表数组a的第二个元素，以此类推。");
    ui->textEdit_code->append("int a[10];");
    ui->textEdit_code->append("for(int i=0;i<9;i++)");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("  a[i]=i;");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("//这是在数组a中通过循环存入了十个数");
    ui->textEdit_code->append("//要注意数组a的第一个位置是0，最后的一个位置是9而不是10");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='array'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='array'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}

void cdex::CONST()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("const是一个C语言的关键字，它限定一个变量不允许被改变。使用const在一定程度上可以提高程序的安全性和可靠性。");
    ui->textEdit->append("(1)可以定义const常量，具有不可变性。");
    ui->textEdit->append("例如:const int Max=100; Max++会产生错误;");
    ui->textEdit->append("(2)便于进行类型检查，使编译器对处理内容有更多了解，消除了一些隐患。");
    ui->textEdit->append("例如: void f(const int i) { .........} 编译器就会知道i是一个常量，不允许修改;");
    ui->textEdit->append("(3)可以避免意义模糊的数字出现，同样可以很方便地进行参数的调整和修改。 同宏定义一样，可以做到不变则已，一变都变!");
    ui->textEdit->append("如(1)中，如果想修改Max的内容，只需要:const int Max=you want;即可!");
    ui->textEdit->append("（4)可以保护被修饰的东西，防止意外的修改，增强程序的健壮性。 还是上面的例子，如果在函数体内修改了i，编译器就会报错;");
    ui->textEdit->append("例如: void f(const int i) { i=10;//error! }");
    ui->textEdit->append("（5） 为函数重载提供了一个参考。");
    ui->textEdit->append("（6） 可以节省空间，避免不必要的内存分配。 例如：");
    ui->textEdit->append("#define PI 3.14159 //常量宏");
    ui->textEdit->append("（7） 提高了效率。 编译器通常不为普通const常量分配存储空间，而是将它们保存在符号表中，这使得它成为一个编译期间的常量，没有了存储与读内存的操作，使得它的效率也很高。");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='const'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='const'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::STATIC()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("在全局变量前，加上关键字static，该变量就被定义成为一个静态全局变量。");
    ui->textEdit->append("静态全局变量有以下特点：");
    ui->textEdit->append("该变量在全局数据区分配内存；");
    ui->textEdit->append("未经初始化的静态全局变量会被程序自动初始化为0（在函数体内声明的自动变量的值是随机的，除非它被显式初始化，而在函数体外被声明的自动变量也会被初始化为0）；");
    ui->textEdit->append("静态全局变量在声明它的整个文件都是可见的，而在文件之外是不可见的；");
    ui->textEdit->append("静态变量都在全局数据区分配内存，包括后面将要提到的静态局部变量。对于一个完整的程序，在内存中的分布情况如下图：");
    ui->textEdit->append("在函数的返回类型前加上static关键字，函数即被定义为静态函数。静态函数与普通函数不同，它只能在声明它的文件当中可见，不能被其它文件使用。");
    ui->textEdit->append("与静态数据成员一样，我们也可以创建一个静态成员函数，它为类的全部服务而不是为某一个类的具体对象服务。");
    ui->textEdit->append("静态成员函数与静态数据成员一样，都是类的内部 实现，属于类定义的一部分。");
    ui->textEdit->append("普通的成员函数一般都隐含了一个this指针，this指针指向类的对象本身，因为普通成员函数总是具体的属于某个类的具体对象的。");
    ui->textEdit->append("但是与普通函数相比，静态成员函数由于不是与任何的对象相联系，因此它不具有this指 针。");
    ui->textEdit->append("从这个意义上讲，它无法访问属于类对象的非静态数据成员，也无法访问非静态成员函数，它只能调用其余的静态成员函数。");
    ui->textEdit->append("静态成员之间可以相互访问，包括静态成员函数访问静态数据成员和访问静态成员函数；");
    ui->textEdit->append("");
    ui->textEdit->append("为什么要引入static?");
    ui->textEdit->append("函数内部定义的变量，在程序执行到它的定义处时，编译器为它在栈上分配空间，大家知道，函数在栈上分配的空间在此函数执行结束时会释放掉，这样就产生了一个问题： ");
    ui->textEdit->append("如果想将函数中此变量的值保存至下一次调用时，如何实现？ 最容易想到的方法是定义一个全局的变量，");
    ui->textEdit->append("但定义为一个全局变量有许多缺点，最明显的缺点是破坏了此变量的访问范围（使得在此函数中定义的变量，不仅仅受此函数控制）。");
    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='static'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='static'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::DEFINE()
{
    /*ui->textEdit->setText("");
    ui->textEdit_code->setText("");
    ui->textEdit->append("#define是C语言中提供的宏定义命令，其主要目的是为程序员在编程时提供一定的方便，并能在一定程度上提高程序的运行效率");
    ui->textEdit->append("它用来将一个标识符定义为一个字符串，该标识符被称为宏名，被定义的字符串称为替换文本。");
    ui->textEdit->append("该命令有两种格式：一种是简单的宏定义，另一种是带参数的宏定义。");
    ui->textEdit->append("(1) 简单的宏定义：");
    ui->textEdit->append("#define <宏名>　　 <字符串>");
    ui->textEdit->append("例： #define PI 3.1415926");
    ui->textEdit->append("(2) 带参数的宏定义");
    ui->textEdit->append("#define <宏名> ( <参数表>) <宏体>");
    ui->textEdit->append("例： #define A(x) x");
    ui->textEdit->append("一个标识符被宏定义后，该标识符便是一个宏名。这时，在程序中出现的是宏名，在该程序被编译前，先将宏名用被定义的字符串替换，这称为宏替换，替换后才进行编译，宏替换是简单的替换。");
    ui->textEdit_code->append("#define N 22");
    ui->textEdit_code->append("void main()");
    ui->textEdit_code->append("{");
    ui->textEdit_code->append("int a=N*N;");
    ui->textEdit_code->append("printf(“%d”,a);");
    ui->textEdit_code->append("}");
    ui->textEdit_code->append("//将N的值定义为22，然后结果就是22的平方");

    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='define'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='define'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);


}
void cdex::POINTER()
{
    /*ui->textEdit->setText("");
    ui->textEdit->append("在计算机科学中，指针（Pointer）是编程语言中的一个对象，利用地址，它的值直接指向（points to）存在电脑存储器中另一个地方的值。由于通过地址能找到所需的变量单元，可以说，地址指向该变量单元。");
    ui->textEdit->append("因此，将地址形象化的称为“指针”。意思是通过它能找到以它为地址的内存单元。");
    ui->textEdit->append("声明:");
    ui->textEdit->append("int* p;//一个指向（还没确定指向哪里）int类型房子的指针p");
    ui->textEdit->append("double* k;//指向double的k");
    ui->textEdit->append("void* p;");
    ui->textEdit->append("");
    ui->textEdit->append("操作:");
    ui->textEdit->append("存入地址:");
    ui->textEdit->append("这里需要用到一个新运算符：&，它称为取地址符，用于获取一个变量的地址。 ");
    ui->textEdit->append("例如：");
    ui->textEdit->append("int *p;");
    ui->textEdit->append("int a;");
    ui->textEdit->append("p=&a;");
    ui->textEdit->append("");
    ui->textEdit->append("输出该地址上的值:");
    ui->textEdit->append("如果你想知道指针所指向位置的值，需要再次用到*。");
    ui->textEdit->append("int *p;");
    ui->textEdit->append("int a;");
    ui->textEdit->append("p=&a;");
    ui->textEdit->append("a=10;");
    ui->textEdit->append("cout<<*p<<endl;");
    ui->textEdit->append("");
    ui->textEdit->append("加/减:");
    ui->textEdit->append("int a=1,*p,*q;");
    ui->textEdit->append("p=&a;");
    ui->textEdit->append("q=p+1;");
    ui->textEdit->append("这相当于把q的地址+1，而不是值+1");
    ui->textEdit->append("");
    ui->textEdit->append("指针数组：");
    ui->textEdit->append("int *p[10];");
    ui->textEdit->append("声明一个有十个指针（p[0],p[1],p[2],…,p[9]）的数组，每个元素都是一个指针。");
    ui->textEdit->append("");
    ui->textEdit->append("函数指针：");
    ui->textEdit->append("你需要知道，在一个程序中，不仅仅是变量需要分配内存，函数也一样，那么函数自然也可以有指针，是函数的入口地址。函数指针声明只比函数声明多一个*和一对括号，例如：");
    ui->textEdit->append("int (*Psum)(int*,int)");
    ui->textEdit->append("其中吧*Psum括起来的括号一定不能少，不然编译器会认为你声明了一个叫Psum的函数，返回类型是int*。");
    ui->textEdit->append("函数指针可以用指针来代替函数名，进行函数的调用");

    ui->textEdit ->moveCursor(QTextCursor::Start, QTextCursor::MoveAnchor);*/
    QSqlQueryModel *model1=new QSqlQueryModel;
    QSqlQueryModel *model2=new QSqlQueryModel;
    QString sql1="select word from yufa where name='pointer'";
    model1->setQuery(sql1);
    QModelIndex a=model1->index(0,0);

    QString word=a.data().toString();
    ui->textEdit_word->setText(word);
    QString sql2="select code from yufa where name='pointer'";
    model2->setQuery(sql2);
    QModelIndex a2=model2->index(0,0);

    QString word2=a2.data().toString();
    ui->textEdit_code->setText(word2);

}
void cdex::showhead()
{
    author a;
    a.headinfoshow();
    close();
}
void cdex::showshuju()
{
    author a;
    a.shujushow();
    close();
}
void cdex::showfunc()
{
    author a;
    a.funcshow();
    close();
}
void cdex::invite_set()
{
    int INFO;

    QSettings invi("./data.ini",QSettings::IniFormat);
    QString info=invi.value("/cdex/if").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="if";
    }
    info=invi.value("/cdex/while").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="while";
    }
    info=invi.value("/cdex/for").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="for";
    }
    info=invi.value("/cdex/switch").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="switch";
    }
    info=invi.value("/cdex/struct").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="struct";
    }
    info=invi.value("/cdex/namespace").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="namespace";
    }
    info=invi.value("/cdex/new").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="new";
    }
    info=invi.value("/cdex/union").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="union";
    }
    info=invi.value("/cdex/array").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="array";
    }
    info=invi.value("/cdex/enum").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="enum";
    }
    info=invi.value("/cdex/pointer").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="pointer";
    }
    info=invi.value("/cdex/function").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="function";
    }
    info=invi.value("/cdex/const").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="const";
    }
    info=invi.value("/cdex/static").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="static";
    }
    info=invi.value("/cdex/define").toString();

    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="define";
    }
}
void cdex::invite()//根据invite set获取到的最大值，推荐最大值对应的搜索项
{
    if(maxS=="if")
    {
        IF();
    }
    else if(maxS=="while")
    {
        WHILE();
    }
    else if(maxS=="array")
    {
        ARRAY();
    }
    else if(maxS=="for")
    {
        FOR();
    }
    else if(maxS=="switch")
    {
        SWITCH();
    }
    else if(maxS=="struct")
    {
        STRUCT();
    }
    else if(maxS=="namespace")
    {
        NAMESPACE();
    }
    else if(maxS=="new")
    {
        NEW();
    }
    else if(maxS=="union")
    {
        UNION();
    }
    else if(maxS=="enum")
    {
        ENUM();
    }
    else if(maxS=="pointer")
    {
        POINTER();
    }
    else if(maxS=="const")
    {
        CONST();
    }
    else if(maxS=="static")
    {
        STATIC();
    }
    else if(maxS=="define")
    {
        DEFINE();
    }
}
void cdex::invite_judge()
{
    invite_set();
    if(max!=0)
    {
       invite();
       QString a1;
       a1="你最常查看的搜索项是："+maxS+"\n现在开始自动推送";
       QMessageBox::about(NULL, "智能推荐", a1);

    }
}
void cdex::showcreate()
{
    hide();
    author a;
    a.createshow();
}
void cdex::showtest()
{
    hide();
    author a;
    a.questionshow();
}
void cdex::knn_cdex()
{
    QVariant data=ui->tableView->currentIndex().data();
    QString current=data.toString();//目前鼠标点击的项
    float cdex_x,cdex_y;//需要插入的坐标值
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql_origin,sql_direction,sql_update;//先查需要插的值是多少，再写新的语句插入
    sql_origin="select x,y from yufa where name='"+current+"'";
    model->setQuery(sql_origin);//结果为一行两列的坐标值.
    QModelIndex indexx,indexy,indexsum;
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    cdex_x=indexx.data().toFloat();
    cdex_y=indexy.data().toFloat();
   //qDebug()<<cdex_x;
    //qDebug()<<cdex_y;
    float currentx,currenty,currentsum;
    float final_x,final_y,final_sum;//最终值
    sql_direction="select x,y,sum from knn where name='cdex'";
    model->setQuery(sql_direction);
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    indexsum=model->index(0,2);
    currentx=indexx.data().toFloat();
    currenty=indexy.data().toFloat();
    currentsum=indexsum.data().toFloat();
    final_x=currentx+cdex_x;
    final_y=currenty+cdex_y;
    final_sum=currentsum+1;//点击一次，就加一次，因为记录的就是统计的次数
    QString a,b,c;
    a=QString::number(final_x);
    b=QString::number(final_y);
    c=QString::number(final_sum);
    sql_update="update knn set x='"+a+"',y='"+b+"',sum='"+c+"' where name='cdex'";
    model->setQuery(sql_update);
}
void cdex::knn_show()
{
    fin.open("f:\\smartC\\knn_cdex.txt", ios::in);
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql;
    QModelIndex indexx,indexy,indexsum;
    sql="select x,y,sum from knn where name='cdex'";
    model->setQuery(sql);
    indexx=model->index(0,0);
    indexy=model->index(0,1);
    indexsum=model->index(0,2);
    float tempx,tempy,tempsum;
    tempx=indexx.data().toFloat();
    tempy=indexy.data().toFloat();
    tempsum=indexsum.data().toFloat();

    float knn_x,knn_y;//坐标
    knn_x=tempx;
    knn_y=tempy;
    KNN knn;
    knn.knn_input(knn_x,knn_y);
    knn.get_all_distance();
    knn.show();
    KN=knn.get_max_fre_label();

    qDebug()<<KN;
    knn_judge();
    fin.close();
}
void cdex::knn_judge()
{
    QString judge;//结果
    switch (KN) {
    case 'a':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐array");
        ARRAY();
        break;
    case 'f':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐for");
        FOR();
        break;
    case 'w':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐while");
        WHILE();
        break;
    case 'i':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐if");
        IF();
        break;

    case 'h':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐switch");
        SWITCH();
        break;
    case 'n':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐new");
        NEW();
        break;
    case 'p':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐pointer");
        POINTER();
        break;

    case 'd':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐define");
        DEFINE();
        break;
    case 'c':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐const");
        CONST();
        break;
    case 'm':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐namespace");
        NAMESPACE();
        break;
    case 's':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐static");
        STATIC();
        break;
    case 'z':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐struct");
        STRUCT();
        break;
    case 'e':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐enum");
        ENUM();
        break;
    case 'u':
        QMessageBox::about(NULL,"提示","根据您的搜索习惯向您推荐union");
        UNION();
        break;
    }
}
void cdex::knn_cdex_init()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql_find,sql_update;//一个查找，一个修改
    QModelIndex indexx,indexy,indexsum;
    sql_find="select x,y,sum from knn where name='cdex'";
    model->setQuery(sql_find);
    indexx=model->index(0,0);//x
    indexy=model->index(0,1);//y
    indexsum=model->index(0,2);//sum
    float tempx,tempy,tempsum;
    tempx=indexx.data().toFloat();
    tempy=indexy.data().toFloat();
    tempsum=indexsum.data().toFloat();
    int ifzero=indexsum.data().toInt();//判断sum是否为0!!不能x/0否则数据库会down掉
    if(ifzero!=0)
    {
     tempx=tempx/tempsum;
    tempy=tempy/tempsum;
     tempsum=0;
     QString tempxx,tempyy,tempsums;
   tempxx=QString::number(tempx);
      tempyy=QString::number(tempy);
     tempsums=QString::number(tempsum);
      sql_update="update knn set x='"+tempxx+"',y='"+tempyy+"',sum='"+tempsums+"' where name='cdex'";
       model->setQuery(sql_update);
     }
}
