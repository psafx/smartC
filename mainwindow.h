﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "author.h"
#include <QMainWindow>
#include "cdex.h"
#include "headinfo.h"
#include"shujujiegou.h"
#include"initial.h"
#include"question.h"
#include<QTcpSocket>
#include<QDataStream>
#include<QByteArray>
#include<QtNetwork>
#include"cmd.h"
#pragma execution_character_set("utf-8")
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void init_knn();//程序每次打开都对knn的上一次数据进行处理
private slots:
    void on_pushButton_exit_clicked();
    void showauthor();


    void on_pushButton_language_clicked();

    void on_pushButton_head_clicked();

    void on_pushButton_shujujiegou_clicked();

    void on_pushButton_function_clicked();

    void on_pushButton_init_clicked();

    void on_pushButton_autocreate_clicked();

    void on_pushButton_test_clicked();
    void openGitee();//打开码云主页
    void checkUpdate();//检查更新
    void newConnect();
    void readmessage();
    void displayError(QAbstractSocket::SocketError);
    void currentVersion();
    void on_pushButton_cmd_clicked();

private:
    Ui::MainWindow *ui;
    author *a;
    initial *init;
    //cdex *Cdex;
    headinfo *head;
    question *quest;
    quint16 blocksize;
    QTcpSocket *tcpSocket;
    QString host,net;//主机ip，端口
    QString message;
    cmd *CMD;
};

#endif // MAINWINDOW_H
