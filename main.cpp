﻿#include "mainwindow.h"
#include <QApplication>
#include<QDebug>
#include<QSplashScreen>//Qt启动画面支持
#include"cmd.h"
#include<QStyleFactory>
bool opendatabase();
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPixmap pixmap("./image/splash.png");
    QSplashScreen splash(pixmap);

    splash.show();
    a.processEvents();
    opendatabase();
    MainWindow w;
    w.setWindowFlags(w.windowFlags()&~Qt::WindowMinMaxButtonsHint|Qt::WindowMinimizeButtonHint);//使窗体可以最小化但不能最大化
    w.show();
    splash.close();
    //QApplication::setStyle(QStyleFactory::create("Fusion"));//设置QT的页面风格

    return a.exec();
}
bool opendatabase()
{
    QSqlDatabase mydb=QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("F:\\smartC\\Qt.db");//平时debug正常用
    //mydb.setDatabaseName("./Qt.db");//release用
    if(mydb.open())
    {
        qDebug()<<"open success";
        return true;
    }
    else
    {
        qDebug()<<"open failed";
        return false;
    }
}
