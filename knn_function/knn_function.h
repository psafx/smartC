﻿#ifndef KNN_FUNCTION_H
#define KNN_FUNCTION_H
#include"knn.h"
#include <QMainWindow>

namespace Ui {
class knn_function;
}

class knn_function : public QMainWindow
{
    Q_OBJECT

public:
    explicit knn_function(QWidget *parent = 0);
    ~knn_function();
    void Myclose();
private slots:
    void on_pushButton_clicked();

private:
    Ui::knn_function *ui;
};

#endif // KNN_FUNCTION_H
