﻿#ifndef KNN_H
#define KNN_H
#include<iostream>
#include<map>
#include<vector>
#include<stdio.h>
#include<cmath>
#include<algorithm>
#include<fstream>
#include<cstdlib>
#include<QSqlQueryModel>
using namespace std;

#define maxRow 12
#define maxCol 2

typedef pair<int, double> PAIR;



class KNN
{
private:
    int k;
    double dataSet[maxRow][maxCol];
    char   labels[maxRow];
    double testData[maxCol];
    map<int, double> map_index_dis;
    map<char, int> map_label;
    double get_distance(double* dt1, double* dt2);

public:
    KNN();
    void get_all_distance();
    void get_max_fre_label();
    void show();
    struct CmpByValue
    {
        bool operator() (const PAIR& lhs, const PAIR& rhs)
        {
            return lhs.second < rhs.second;
        }

    };
     ifstream fin;
};

#endif // KNN_H
