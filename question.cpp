﻿#include "question.h"
#include "ui_question.h"
#include<time.h>
#include<QSqlQueryModel>
#include<vector>
#include<QString>
#include<QDebug>
#include<QMessageBox>
#include<QColorDialog>
#include<algorithm>
#include"author.h"
#pragma execution_character_set("utf-8")
question::question(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::question)
{
    ui->setupUi(this);
    //为该窗口设计美化
    QPixmap pixmap = QPixmap("./image/question.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    setFixedSize(800,600);//设定窗口大小为800*600
    ui->pushButton_enter->setIcon(QIcon("./image/enter.ico"));
     ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));
     ui->stackedWidget->setCurrentIndex(0);//设置初始的画面
     rand();
     connect(ui->action_cdex,SIGNAL(triggered(bool)),this,SLOT(showcdex()));
     connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(showshuju()));
     connect(ui->action_func,SIGNAL(triggered(bool)),this,SLOT(showfunc()));
     connect(ui->action_create,SIGNAL(triggered(bool)),this,SLOT(showcreate()));
     connect(ui->action_head,SIGNAL(triggered(bool)),this,SLOT(showhead()));
     ui->action_cdex->setIcon(QIcon("./image/opencdex.ico"));
     ui->action_head->setIcon(QIcon("./image/openhead.ico"));
     ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
     ui->action_func->setIcon(QIcon("./image/openfunc.ico"));
     ui->action_create->setIcon(QIcon("./image/create.ico"));
}


question::~question()
{
    delete ui;
}

void question::on_pushButton_exit_clicked()
{
    close();
}

void question::on_pushButton_enter_clicked()
{

    //section=ui->comboBox_section->currentText();
    //diffculty=ui->comboBox_difficulty->currentText();
    //number=ui->comboBox_num->currentText();//获取三个组合框中文本的内容
    ui->stackedWidget->setCurrentIndex(1);//设置点击确认之后进入答题页面
    QPixmap pixmap = QPixmap("./image/answer.png").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette.setBrush(QPalette::Background, QBrush(pixmap));
    this->setPalette( palette );//窗口背景图片 终
    SetQuestion(now);
}
void question::rand()//随机取题目函数
{
    for(int i=0;i<10;i++)
    {
        questionnum[i]=0;
    }
    qsrand(time(0));
    int sum=0;//数据库中符合用户要求的题目数量
    QString sql;//数据库语句
    QSqlQueryModel *model=new QSqlQueryModel;
    QModelIndex Index;
    if(ui->comboBox_section->currentText()=="全部")
    {
        sql="select count(*) from test";//求出题库一共有多少道题
        model->setQuery(sql);
        Index=model->index(0,0);//取出第1行第1列的数据，即之前的统计结果
        sum=Index.data().toInt();
    }
    else
    {
        sql="select count(*) from test where name='"+ui->comboBox_section->currentText()+"'";
        model->setQuery(sql);
        Index=model->index(0,0);//取出第1行第1列的数据，即之前的统计结果
        sum=Index.data().toInt();
    }
    int *a=new int[sum];//生成长度为sum的动态数组
    for(int i=1;i<=sum;i++)
    {
        a[i-1]=i;//注意下标要修改
    }
    int r=qrand();
    int temp;
    for(int i=sum-1;i>=1;i--)
    {
        temp=a[i-1];
        a[i-1]=a[r%i];
        a[r%i]=temp;
    }
    if(sum<=10)
    {
        for(int i=0;i<sum;i++)
        {
            questionnum[i]=a[i];//用户取到了十道题，直接赋值即可
            //qDebug()<<a[i];
        }
    }
    else//sum>10
    {
        for(int i=0;i<10;i++)
        {
            questionnum[i]=a[i];//只取到10
        }
    }
    /*for(int i=0;i<sum;i++)
    {
        qDebug()<<questionnum[i];
    }*/
    now=1;//当前的题目数为1
    delete[] a;//用完删除
    button_init();
}
void question::SetQuestion(int now)//根据之前的随机数和用户选择进行出题
{
    qDebug()<<questionnum[now-1];
    section=ui->comboBox_section->currentText();
    diffculty=ui->comboBox_difficulty->currentText();
    number=ui->comboBox_num->currentText();//获取三个组合框中文本的内容
    if(number=="5题")
    {
        Number=5;
        ui->pushButton_6->setDisabled(true);
        ui->pushButton_7->setDisabled(true);
        ui->pushButton_8->setDisabled(true);
        ui->pushButton_9->setDisabled(true);
        ui->pushButton_10->setDisabled(true);

        //如果只选了五道题的话，就让后面的题目按钮设为不可选中
    }
    else
    {
        Number=10;
    }
        ui->pushButton_reason->setDisabled(true);
    qDebug()<<"setQuestion"<<Number;
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    //关于练习的信息在enter按钮的槽函数中
    QString questnum;
    int a=questionnum[now-1];
    questnum=QString::number(a);
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        qDebug()<<"题号为"<<questnum<<"题目信息:"<<quest;

        currentnum=questnum;
}

void question::on_pushButton_next_clicked()
{
    if(now<Number&&now!=Number-1)//如果现在出的题还没到选择的题目数，则继续出题。
    {
        store();
        correctJ();
        now++;

        SetQuestion(now);

    }
    else if(now<Number&&now==Number-1)//先将按钮名修改
    {
        store();
        correctJ();
        ui->pushButton_next->setText("交卷");
        now++;

        SetQuestion(now);

    }
    else
    {
        store();
        correctJ();
        //QMessageBox::question(NULL,"提示","要交卷吗？",QMessageBox::Yes | QMessageBox::No);
        int rnt=QMessageBox::question(NULL,"提示","要交卷吗？",QMessageBox::Yes | QMessageBox::No);
        if(QMessageBox::Yes==rnt)
        {
            judge();
            ui->pushButton_reason->setDisabled(false);
        }
    }
    //qDebug()<<"选择要做的题目数量为："<<Number;//功能完成

}




void question::on_pushButton_1_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[0];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}


void question::on_pushButton_2_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质

    QString questnum;

    int a=questionnum[1];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_3_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[2];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_4_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质

    QString questnum;

    int a=questionnum[3];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_5_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[4];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_6_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[5];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_7_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[6];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_8_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[7];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_9_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[8];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}

void question::on_pushButton_10_clicked()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString quest;//题目信息
    QString A,B,C,D,correct;//题目各选项及正确答案
    QString SqlQuest,SqlA,SqlB,SqlC,SqlD,Sqlcorrect;//关于各信息的检索Sql
    //QString section=ui->comboBox_section->currentText();//目前练习的范围
    //QString diff=ui->comboBox_difficulty->currentText();//目前的难度
    QModelIndex index;//数据库搜索结果存储介质
    QString buttonstr;//按钮上的数字
    QString questnum;

    int a=questionnum[9];
    questnum=QString::number(a);
    qDebug()<<"题号为"<<questnum;
    if(section=="全部")
    {


        SqlQuest="select question from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlA="select A from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlB="select B from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlC="select C from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        SqlD="select D from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
        Sqlcorrect="select correct from test where Allnumber='"+questnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        SqlQuest="select question from test where number='"+questnum+"' and name='"+section+"'";
        SqlA="select A from test where number='"+questnum+"' and name='"+section+"'";
        SqlB="select B from test where number='"+questnum+"' and name='"+section+"'";
        SqlC="select C from test where number='"+questnum+"' and name='"+section+"'";
        SqlD="select D from test where number='"+questnum+"' and name='"+section+"'";
        Sqlcorrect="select correct from test where number='"+questnum+"'and name='"+section+"'";
    }
        model->setQuery(SqlQuest);
        index=model->index(0,0);
        quest=index.data().toString();
        model->setQuery(SqlA);
        index=model->index(0,0);
        A=index.data().toString();
        model->setQuery(SqlB);
        index=model->index(0,0);
        B=index.data().toString();
        model->setQuery(SqlC);
        index=model->index(0,0);
        C=index.data().toString();
        model->setQuery(SqlD);
        index=model->index(0,0);
        D=index.data().toString();
        model->setQuery(Sqlcorrect);
        index=model->index(0,0);
        correct=index.data().toString();
        ui->textEdit_question->setText(quest);
        ui->lineEdit_A->setText(A);
        ui->lineEdit_B->setText(B);
        ui->lineEdit_C->setText(C);
        ui->lineEdit_D->setText(D);
        currentnum=questnum;
}
void question::store()//存选择的答案
{

    if(ui->radioButton_A->isChecked())
    {
        ans[now-1]=1;
    }
    else if(ui->radioButton_B->isChecked())
    {
        ans[now-1]=2;
    }
    else if(ui->radioButton_C->isChecked())
    {
        ans[now-1]=3;
    }
    else if(ui->radioButton_D->isChecked())//最后一个else if不能省略
    {
        ans[now-1]=4;
    }
}
void question::correctJ()//存正确答案
{
    QString questnum;
    QModelIndex index;
    int a=questionnum[now-1];
    questnum=QString::number(a);
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql;
    if(section=="全部")
    {
        sql="select answer from test where Allnumber='"+questnum+"'";

    }
    else
    {
        sql="select answer from test where number='"+questnum+"'";
    }
    model->setQuery(sql);
    index=model->index(0,0);

    int answer;
    answer=index.data().toInt();;
    //qDebug()<<"检查是否存放了正确答案"<<answer;
    correct[now-1]=answer;
}
void question::judge()//判断对错
{
    //Number
    for(int i=0;i<Number;i++)
    {
        if(ans[i]==correct[i])
        {
            answ[i]=1;
        }
        else
        {
            answ[i]=0;
        }
    }
    for(int i=0;i<Number;i++)
    {
        switch (i) {
        case 0:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_1->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_1->setPalette(pal);
                ui->pushButton_1->setAutoFillBackground(true);
                ui->pushButton_1->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_1->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_1->setPalette(pal);
                ui->pushButton_1->setAutoFillBackground(true);
                ui->pushButton_1->setFlat(true);
            }
            break;
        case 1:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_2->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_2->setPalette(pal);
                ui->pushButton_2->setAutoFillBackground(true);
                ui->pushButton_2->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_2->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_2->setPalette(pal);
                ui->pushButton_2->setAutoFillBackground(true);
                ui->pushButton_2->setFlat(true);
            }
            break;
        case 2:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_3->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_3->setPalette(pal);
                ui->pushButton_3->setAutoFillBackground(true);
                ui->pushButton_3->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_3->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_3->setPalette(pal);
                ui->pushButton_3->setAutoFillBackground(true);
                ui->pushButton_3->setFlat(true);
            }
            break;
        case 3:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_4->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_4->setPalette(pal);
                ui->pushButton_4->setAutoFillBackground(true);
                ui->pushButton_4->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_4->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_4->setPalette(pal);
                ui->pushButton_4->setAutoFillBackground(true);
                ui->pushButton_4->setFlat(true);
            }
            break;
        case 4:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_5->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_5->setPalette(pal);
                ui->pushButton_5->setAutoFillBackground(true);
                ui->pushButton_5->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_5->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_5->setPalette(pal);
                ui->pushButton_5->setAutoFillBackground(true);
                ui->pushButton_5->setFlat(true);
            }
            break;
        case 5:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_6->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_6->setPalette(pal);
                ui->pushButton_6->setAutoFillBackground(true);
                ui->pushButton_6->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_6->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_6->setPalette(pal);
                ui->pushButton_6->setAutoFillBackground(true);
                ui->pushButton_6->setFlat(true);
            }
            break;
        case 6:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_7->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_7->setPalette(pal);
                ui->pushButton_7->setAutoFillBackground(true);
                ui->pushButton_7->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_7->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_7->setPalette(pal);
                ui->pushButton_7->setAutoFillBackground(true);
                ui->pushButton_7->setFlat(true);
            }
            break;
        case 7:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_8->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_8->setPalette(pal);
                ui->pushButton_8->setAutoFillBackground(true);
                ui->pushButton_8->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_8->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_8->setPalette(pal);
                ui->pushButton_8->setAutoFillBackground(true);
                ui->pushButton_8->setFlat(true);
            }
            break;
        case 8:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_9->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_9->setPalette(pal);
                ui->pushButton_9->setAutoFillBackground(true);
                ui->pushButton_9->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_9->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_9->setPalette(pal);
                ui->pushButton_9->setAutoFillBackground(true);
                ui->pushButton_9->setFlat(true);
            }
            break;
        case 9:
            if(answ[i]==1)//正确
            {
                QPalette pal = ui->pushButton_10->palette();
                pal.setColor(QPalette::Button,Qt::green);
                ui->pushButton_10->setPalette(pal);
                ui->pushButton_10->setAutoFillBackground(true);
                ui->pushButton_10->setFlat(true);
            }
            else
            {
                QPalette pal = ui->pushButton_10->palette();
                pal.setColor(QPalette::Button,Qt::red);
                ui->pushButton_10->setPalette(pal);
                ui->pushButton_10->setAutoFillBackground(true);
                ui->pushButton_10->setFlat(true);
            }
            break;

        }
        //qDebug()<<"用户提交的答案为"<<ans[i];
        //qDebug()<<"系统的正确答案为"<<correct[i];
        //qDebug()<<"目前的判断结果为(1or0)"<<answ[i];
    }
}


void question::on_pushButton_return_clicked()//返回
{
    int rnt=QMessageBox::question(NULL,"提示","要返回吗？",QMessageBox::Yes | QMessageBox::No);
    if(QMessageBox::Yes==rnt)
    {
        ui->stackedWidget->setCurrentIndex(0);
        //ui->pushButton_next->setText("下一题");//将原本改为交卷的按钮名改回来
        init();
        rand();
    }

}
void question::init()
{
    ui->pushButton_next->setText("下一题");//将原本改为交卷的按钮名改回来
    ui->pushButton_6->setDisabled(false);
    ui->pushButton_7->setDisabled(false);
    ui->pushButton_8->setDisabled(false);
    ui->pushButton_9->setDisabled(false);
    ui->pushButton_10->setDisabled(false);
    ui->pushButton_reason->setDisabled(true);
    button_init();
}
void question::button_init()
{
    QPalette pal1 = ui->pushButton_1->palette();
    pal1.setColor(QPalette::Button,Qt::white);
    ui->pushButton_1->setPalette(pal1);
    ui->pushButton_1->setAutoFillBackground(true);
    ui->pushButton_1->setFlat(true);
    QPalette pal2 = ui->pushButton_2->palette();
    pal2.setColor(QPalette::Button,Qt::white);
    ui->pushButton_2->setPalette(pal2);
    ui->pushButton_2->setAutoFillBackground(true);
    ui->pushButton_2->setFlat(true);
    QPalette pal3 = ui->pushButton_3->palette();
    pal3.setColor(QPalette::Button,Qt::white);
    ui->pushButton_3->setPalette(pal3);
    ui->pushButton_3->setAutoFillBackground(true);
    ui->pushButton_3->setFlat(true);

    QPalette pal4 = ui->pushButton_4->palette();
    pal4.setColor(QPalette::Button,Qt::white);
    ui->pushButton_4->setPalette(pal4);
    ui->pushButton_4->setAutoFillBackground(true);
    ui->pushButton_4->setFlat(true);
    QPalette pal5 = ui->pushButton_5->palette();
    pal5.setColor(QPalette::Button,Qt::white);
    ui->pushButton_5->setPalette(pal5);
    ui->pushButton_5->setAutoFillBackground(true);
    ui->pushButton_5->setFlat(true);

    QPalette pal6 = ui->pushButton_6->palette();
    pal6.setColor(QPalette::Button,Qt::white);
    ui->pushButton_6->setPalette(pal2);
    ui->pushButton_6->setAutoFillBackground(true);
    ui->pushButton_6->setFlat(true);
    QPalette pal7 = ui->pushButton_7->palette();
    pal7.setColor(QPalette::Button,Qt::white);
    ui->pushButton_7->setPalette(pal2);
    ui->pushButton_7->setAutoFillBackground(true);
    ui->pushButton_7->setFlat(true);

    QPalette pal8 = ui->pushButton_8->palette();
    pal8.setColor(QPalette::Button,Qt::white);
    ui->pushButton_8->setPalette(pal2);
    ui->pushButton_8->setAutoFillBackground(true);
    ui->pushButton_8->setFlat(true);

    QPalette pal9 = ui->pushButton_9->palette();
    pal9.setColor(QPalette::Button,Qt::white);
    ui->pushButton_9->setPalette(pal9);
    ui->pushButton_9->setAutoFillBackground(true);
    ui->pushButton_9->setFlat(true);
    QPalette pal10 = ui->pushButton_10->palette();
    pal10.setColor(QPalette::Button,Qt::white);
    ui->pushButton_10->setPalette(pal10);
    ui->pushButton_10->setAutoFillBackground(true);
    ui->pushButton_10->setFlat(true);
}

void question::on_pushButton_reason_clicked()//显示答案
{
    //currentnum
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sql;
    if(section=="全部")
    {
        sql="select reason from test where Allnumber='"+currentnum+"' and difficulty='"+diffculty+"'";
    }
    else
    {
        sql="select reason from test where number='"+currentnum+"' and difficulty='"+diffculty+"'";
    }
    model->setQuery(sql);
    QModelIndex index=model->index(0,0);
    QString result=index.data().toString();
    if(result!="")
    {
        QMessageBox::about(NULL,"解析",result);
    }
    else
    {
        QMessageBox::about(NULL,"解析","暂无解析");
    }
}

void question::on_pushButton_info_clicked()//显示题库的题目情况
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString sqlall,sqlshuju,sqlyuedu,sqlsuanfa,sqljichu;
    QString all,shuju,yuedu,suanfa,jichu;
    QModelIndex index;
    sqlall="select count(*) from test ";
    sqlshuju="select count(*) from test where name='数据结构'";
    sqljichu="select count(*) from test where name='C++基础'";
    sqlsuanfa="select count(*) from test where name='算法'";
    sqlyuedu="select count(*) from test where name='程序阅读'";
    model->setQuery(sqlall);
    index=model->index(0,0);
    all=index.data().toString();

    model->setQuery(sqlshuju);
    index=model->index(0,0);
    shuju=index.data().toString();

    model->setQuery(sqlsuanfa);
    index=model->index(0,0);
    suanfa=index.data().toString();

    model->setQuery(sqljichu);
    index=model->index(0,0);
    jichu=index.data().toString();

    model->setQuery(sqlyuedu);
    index=model->index(0,0);
    yuedu=index.data().toString();

    QString show;//显示信息
    show="题目总数为:"+all+"\n"+"C++基础题目数为:"+jichu+"\n"+"程序阅读题目数为:"+yuedu+"\n"+"算法题目数为:"+suanfa+"\n"+"数据结构题目数为:"+shuju;
    QMessageBox::about(NULL,"提示",show);
}
void question::showcdex()
{
    hide();
    author a;
    a.cdexshow();
}
void question::showcreate()
{
    hide();
    author a;
    a.createshow();
}
void question::showfunc()
{
    hide();
    author a;
    a.funcshow();
}
void question::showshuju()
{
    hide();
    author a;
    a.shujushow();
}
void question::showhead()
{
    hide();
    author a;
    a.headinfoshow();
}
