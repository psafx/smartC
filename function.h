﻿#ifndef FUNCTION_H
#define FUNCTION_H

#include <QMainWindow>

namespace Ui {
class function;
}

class function : public QMainWindow
{
    Q_OBJECT

public:
    explicit function(QWidget *parent = 0);
    ~function();
    void init();
    void Show();
    void voidfunc();
    void havefunc();//带参函数
    void YINYONG();//引用
    void ZHIZHENFUNC();//指针函数
    void DIGUI();
    void XUHANSHU();
    void CHUNXUHANSHU();
    void JILEI();//基类
    void FRIEND();
    void CLASS();
    void GOUZAO();
    void XIGOU();
    void CHONGZAI();
    void invite_set();
    void invite();
    void invite_judge();
    void knn_add();//每次点击就加坐标值
    void knn_function_init();
    void knn_function_judge();//判断推荐哪个搜索项
private slots:
    void on_pushButton_exit_clicked();

    void on_tableView_clicked(const QModelIndex &index);
    void showyufa();
    void showshuju();
    void showhead();
    void showcreate();
    void showtest();
    void on_pushButton_change_clicked();

private:
    Ui::function *ui;
    QString maxS;
    int max=0;
    QString data;//当前正在显示的搜索项名称
};

#endif // FUNCTION_H
