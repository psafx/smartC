#-------------------------------------------------
#
# Project created by QtCreator 2018-06-23T18:02:54
#
#-------------------------------------------------

QT       += core gui
QT       += core gui sql
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
RC_ICONS = tubiao.ico
TARGET = smartC
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    author.cpp \
    cdex.cpp \
    headinfo.cpp \
    function.cpp \
    shujujiegou.cpp \
    initial.cpp \
    createcode.cpp \
    remind.cpp \
    question.cpp \
    cmd.cpp

HEADERS += \
        mainwindow.h \
    author.h \
    cdex.h \
    headinfo.h \
    function.h \
    shujujiegou.h \
    initial.h \
    createcode.h \
    remind.h \
    question.h \
    cmd.h \
    knn.h

FORMS += \
        mainwindow.ui \
    author.ui \
    cdex.ui \
    headinfo.ui \
    function.ui \
    shujujiegou.ui \
    initial.ui \
    createcode.ui \
    remind.ui \
    question.ui \
    cmd.ui

DISTFILES +=
