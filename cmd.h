﻿#ifndef CMD_H
#define CMD_H
#include"author.h"
#include <QWidget>
#include<QVector>
namespace Ui {
class cmd;
}

class cmd : public QWidget
{
    Q_OBJECT

public:
    explicit cmd(QWidget *parent = 0);
    ~cmd();
    bool eventFilter(QObject *target, QEvent *event);//事件过滤器
    void send();//发送信息至textEdit
    void judge();
private slots:
    void setCursor();
private:
    Ui::cmd *ui;
    QVector<QString> command;
    author a;
};

#endif // CMD_H
